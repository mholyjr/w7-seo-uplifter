<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', '#jform_catid', null, array('disable_search_threshold' => 0 ));
JHtml::_('formbehavior.chosen', '#jform_tags', null, array('placeholder_text_multiple' => JText::_('JGLOBAL_TYPE_OR_SELECT_SOME_TAGS')));
JHtml::_('formbehavior.chosen', 'select');
$this->configFieldsets  = array('editorConfig');

//print_r($this->item);

?>
<form action="" id="adminForm" method="post" name="adminForm" class="w7p_item_edit_form w7p_form">
    <div class="span2">
		<div id="j-sidebar-container">
			<?php echo JHtmlSidebar::render(); ?>
			<div class="logo_container">
				<a href="https://www.w7extensions.com" target="_blank"><img class="admin_logo" src="<?php echo JURI::root() . "administrator/components/com_w7seouplifter/assets/images/w7extensions_logo_color.svg"; ?>"></a>
				<a href="https://www.w7extensions.com" target="_blank">www.w7extensions.com</a>
			</div>
		</div>
    </div>
    <div id="j-main-container" class="span10 dashboard">
        <div class="row-fluid">
            <div class="span9 w7p_main_card">
                <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'htaccess')); ?>
                    <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'htaccess', JText::_('COM_W7SEOUPLIFTER_EDITOR_LABEL')); ?>
                        <div class="form-vertical">
                            <fieldset class="adminform">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <?php 
                                            foreach($this->form->getFieldset('') as $field) {
                                                echo $field->renderField();
                                            }
                                        ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    <?php echo JHtml::_('bootstrap.endTab'); ?>

                <?php echo JHtml::_('bootstrap.endTabSet'); ?>
            </div>
            <div class="span3 w7p_edit_sidebar">
                <div class="w7p_card">
                    <div class="w7p_card_title">
                        <h3><?php echo JText::_('COM_W7SEOUPLIFTER_HTACCESS_WARNING'); ?></h3>
                    </div>
                    <fieldset class="adminform">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="page_summary">
                                    <div class="section" style="margin-bottom: 0;">
                                        <div class="row-fluid d-flex align-items-start summary_row warning ?>">
                                            <div class="span2 text-center">
                                                <i class="message_icon bi bi-exclamation-circle warning"></i>
                                            </div>
                                            <div class="span10 d-flex summary_message">
                                                <p><?php echo JText::_('COM_W7SEOUPLIFTER_HTACCESS_WARNING_MESSAGE'); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="jform[id]" id="id" value="1">
    <input type="hidden" name="task" value=""/>
    <?php echo JHtml::_('form.token'); ?>
</form>