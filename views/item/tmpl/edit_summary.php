<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

JLoader::register('W7SeoUplifterSummaryHelper', JPATH_COMPONENT . '/helpers/summary.php');

$helper = new W7SeoUplifterSummaryHelper;
$title = $helper->checkTitle($this->item->get('item_title'));
$description = $helper->checkDescription($this->item->get('item_description'));
$fTitle = $helper->checkSocialTitle($this->item->get('facebook')['og_title']);
$tTitle = $helper->checkSocialTitle($this->item->get('twitter')['twitter_title']);
$fImage = $helper->checkSocialImage($this->item->get('facebook')['og_image']);
$tImage = $helper->checkSocialImage($this->item->get('twitter')['twitter_image']);
$fDescription = $helper->checkSocialDescription($this->item->get('facebook')['og_description']);
$tDescription = $helper->checkSocialDescription($this->item->get('twitter')['twitter_description']);

?>

<div class="page_summary">
    <div class="section">
        <div class="control-label">
            <label>
                <?php echo JText::_('COM_W7SEOUPLIFTER_ITEM_TITLE_LABEL'); ?>
            </label>
        </div>
        <?php foreach($title as $i) : ?>
            <div class="row-fluid d-flex align-items-start summary_row <?php echo $i['type']; ?>">
                <div class="span2 text-center">
                    <i class="message_icon bi <?php echo $i['icon'] . ' ' . $i['type']; ?>"></i>
                </div>
                <div class="span10 d-flex summary_message">
                    <p><?php echo $i['message']; ?></p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <div class="section">
        <div class="control-label">
            <label>
                <?php echo JText::_('COM_W7SEOUPLIFTER_ITEM_DESCRIPTION_LABEL'); ?>
            </label>
        </div>
        <?php foreach($description as $i) : ?>
            <div class="row-fluid d-flex align-items-start summary_row <?php echo $i['type']; ?>">
                <div class="span2 text-center">
                    <i class="message_icon bi <?php echo $i['icon'] . ' ' . $i['type']; ?>"></i>
                </div>
                <div class="span10 d-flex summary_message">
                    <p><?php echo $i['message']; ?></p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <div class="section">
        <div class="control-label">
            <label>
                <?php echo JText::_('COM_W7SEOUPLIFTER_OG_TAGS_LABEL'); ?>
            </label>
        </div>
        <?php foreach($fTitle as $i) : ?>
            <div class="row-fluid d-flex align-items-start summary_row <?php echo $i['type']; ?>">
                <div class="span2 text-center">
                    <i class="message_icon bi <?php echo $i['icon'] . ' ' . $i['type']; ?>"></i>
                </div>
                <div class="span10 d-flex summary_message">
                    <p><?php echo $i['message']; ?></p>
                </div>
            </div>
        <?php endforeach; ?>
        <?php foreach($fImage as $i) : ?>
            <div class="row-fluid d-flex align-items-start summary_row <?php echo $i['type']; ?>">
                <div class="span2 text-center">
                    <i class="message_icon bi <?php echo $i['icon'] . ' ' . $i['type']; ?>"></i>
                </div>
                <div class="span10 d-flex summary_message">
                    <p><?php echo $i['message']; ?></p>
                </div>
            </div>
        <?php endforeach; ?>
        <?php foreach($fDescription as $i) : ?>
            <div class="row-fluid d-flex align-items-start summary_row <?php echo $i['type']; ?>">
                <div class="span2 text-center">
                    <i class="message_icon bi <?php echo $i['icon'] . ' ' . $i['type']; ?>"></i>
                </div>
                <div class="span10 d-flex summary_message">
                    <p><?php echo $i['message']; ?></p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <div class="section">
        <div class="control-label">
            <label>
                <?php echo JText::_('COM_W7SEOUPLIFTER_TWITTER_LABEL'); ?>
            </label>
        </div>
        <?php foreach($tTitle as $i) : ?>
            <div class="row-fluid d-flex align-items-start summary_row <?php echo $i['type']; ?>">
                <div class="span2 text-center">
                    <i class="message_icon bi <?php echo $i['icon'] . ' ' . $i['type']; ?>"></i>
                </div>
                <div class="span10 d-flex summary_message">
                    <p><?php echo $i['message']; ?></p>
                </div>
            </div>
        <?php endforeach; ?>
        <?php foreach($tImage as $i) : ?>
            <div class="row-fluid d-flex align-items-start summary_row <?php echo $i['type']; ?>">
                <div class="span2 text-center">
                    <i class="message_icon bi <?php echo $i['icon'] . ' ' . $i['type']; ?>"></i>
                </div>
                <div class="span10 d-flex summary_message">
                    <p><?php echo $i['message']; ?></p>
                </div>
            </div>
        <?php endforeach; ?>
        <?php foreach($tDescription as $i) : ?>
            <div class="row-fluid d-flex align-items-start summary_row <?php echo $i['type']; ?>">
                <div class="span2 text-center">
                    <i class="message_icon bi <?php echo $i['icon'] . ' ' . $i['type']; ?>"></i>
                </div>
                <div class="span10 d-flex summary_message">
                    <p><?php echo $i['message']; ?></p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
