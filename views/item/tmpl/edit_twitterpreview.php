<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

$urlParts = parse_url(JURI::root());
$domain = preg_replace('/^www\./', '', $urlParts['host']);

?>
<script>
    jQuery(document).ready(function() {

        var title = jQuery('#jform_twitter_twitter_title').val()
        jQuery('#jform_twitter_twitter_title_prev').html(title)

        var desc = jQuery('#jform_twitter_twitter_description').val()
        jQuery('#jform_twitter_twitter_description_preview').html(desc)

        var img = jQuery('#jform_twitter_twitter_image').val()
        jQuery('#jform_twitter_twitter_image_prev').css('background-image', 'url(<?php echo JURI::root(); ?>' + img + ')')

        jQuery('#jform_twitter_twitter_title').on('input', function() {
            var val = jQuery(this).val()
            jQuery('#jform_twitter_twitter_title_prev').html(val)
        })

        jQuery('#jform_twitter_twitter_description').on('input', function() {
            var val = jQuery(this).val()
            jQuery('#jform_twitter_twitter_description_preview').html(val)
        })

        jQuery('#jform_twitter_twitter_image').on('change', function() {
            var img = jQuery(this).val()
            jQuery('#jform_twitter_twitter_image_prev').css('background-image', 'url(<?php echo JURI::root(); ?>' + img + ')')
        })
    })
    
</script>
<div>
<div class="control-label">
    <label>
        <?php echo JText::_('COM_W7SEOUPLIFTER_TWITTER_PREVIEW'); ?> <span class="small" style="font-weight: 400;"><?php echo JText::_('COM_W7SEOUPLIFTER_TWITTER_PREVIEW_DISCLAIMER'); ?></span>
    </label>
</div>
<div class="card-seo-twitter">
    <div class="card-seo-twitter__image js-preview-image" id="jform_twitter_twitter_image_prev" style="background-image: url('https://www.web7.cz/images/banners/web7-system-webove-stranky-tvorba.jpg');"></div>
    <div class="card-seo-twitter__text">
        <span id="jform_twitter_twitter_title_prev" class="card-seo-twitter__title js-preview-title">Add title</span>
        <span id="jform_twitter_twitter_description_preview" class="card-seo-twitter__description js-preview-description">Add description</span>
        <span class="card-seo-twitter__link js-preview-domain"><?php echo $domain; ?></span>
    </div>
</div>
</div>