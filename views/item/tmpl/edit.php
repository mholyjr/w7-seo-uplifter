<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', '#jform_catid', null, array('disable_search_threshold' => 0 ));
JHtml::_('formbehavior.chosen', '#jform_tags', null, array('placeholder_text_multiple' => JText::_('JGLOBAL_TYPE_OR_SELECT_SOME_TAGS')));
JHtml::_('formbehavior.chosen', 'select');
$this->configFieldsets  = array('editorConfig');

?>
<form class="w7p_item_edit_form w7p_form" action="<?php echo JRoute::_('index.php?option=com_w7seouplifter&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row-fluid">
        <div class="span9 w7_top_input">
            <div class="row-fluid">
                <div class="form-horizontal"> 
                <div class="span6">
                    <?php 
                        foreach($this->form->getFieldset('menuitem') as $field) {
                            echo $field->renderField();
                        }
                    ?>
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span9 w7p_main_card">
            <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'meta')); ?>
                <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'meta', JText::_('COM_W7SEOUPLIFTER_META_TAGS_LABEL')); ?>
                    <div class="form-vertical">
                        <fieldset class="adminform">
                            <div class="row-fluid">
                                <div class="span5">
                                    <?php 
                                        foreach($this->form->getFieldset('meta') as $field) {
                                            echo $field->renderField();
                                        }
                                    ?>
                                </div>
                                <div class="span7 d-flex justify-content-center">
                                    <?php echo $this->loadTemplate('serp'); ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                <?php echo JHtml::_('bootstrap.endTab'); ?>

                <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'ogtags', JText::_('COM_W7SEOUPLIFTER_OG_TAGS_LABEL')); ?>
                    <div class="form-vertical">
                        <fieldset class="adminform">
                            <div class="row-fluid">
                                <div class="span5">
                                    <?php 
                                        foreach($this->form->getFieldset('facebookParams') as $field) {
                                            echo $field->renderField();
                                        }
                                    ?>
                                </div>
                                <div class="span7 d-flex justify-content-center">
                                    <?php echo $this->loadTemplate('facebookpreview'); ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                <?php echo JHtml::_('bootstrap.endTab'); ?>

                <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'twitter', JText::_('COM_W7SEOUPLIFTER_TWITTER_LABEL')); ?>
                    <div class="form-vertical">
                        <fieldset class="adminform">
                            <div class="row-fluid">
                                <div class="span5">
                                    <?php 
                                        foreach($this->form->getFieldset('twitterParams') as $field) {
                                            echo $field->renderField();
                                        }
                                    ?>
                                </div>
                                <div class="span7 d-flex justify-content-center">
                                    <?php echo $this->loadTemplate('twitterpreview'); ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php echo JHtml::_('bootstrap.endTabSet'); ?>
        </div>

        <div class="span3 w7p_edit_sidebar">
            <div class="w7p_card">
                <div class="w7p_card_title">
                    <h3><?php echo JText::_('COM_W7SEOUPLIFTER_OTHER_PROPERTIES'); ?></h3>
                </div>
                <fieldset class="adminform">
                    <div class="row-fluid">
                        <div class="span12">
                            <?php 
                                foreach($this->form->getFieldset('state') as $field) {
                                    echo $field->renderField();
                                }
                            ?>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="w7p_card">
                <div class="w7p_card_title">
                    <h3><?php echo JText::_('COM_W7SEOUPLIFTER_MENU_ITEM_SUMMARY'); ?></h3>
                </div>
                <fieldset class="adminform">
                    <div class="row-fluid">
                        <div class="span12">
                            <?php echo $this->loadTemplate('summary'); ?>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>

    </div>
    <input type="hidden" name="task" value="item.edit" />
    <?php echo JHtml::_('form.token'); ?>
</form>