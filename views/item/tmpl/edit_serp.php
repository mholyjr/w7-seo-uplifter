<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

$urlParts = parse_url(JURI::root());
$domain = preg_replace('/^www\./', '', $urlParts['host']);

?>
<script>
    jQuery(document).ready(function() {

        var title  = jQuery('#jform_item_title').val()
        jQuery('.w7_serp_title_wrapper .title').html(title)

        var desc  =  jQuery('#jform_item_description').val()
        jQuery('.w7_serp_desc_wrapper .append_desc').html(desc)

        jQuery('#jform_item_title').on('input', function() {
            var val  = jQuery(this).val()
            jQuery('.w7_serp_title_wrapper .title').html(val)
        })

        jQuery('#jform_item_description').on('input', function() {
            var val  = jQuery(this).val()
            jQuery('.w7_serp_desc_wrapper .append_desc').html(val)
        })
    })
    
</script>
<div>
<div class="control-label">
    <label>
        <?php echo JText::_('COM_W7SEOUPLIFTER_SERP_PREVIEW'); ?> <span class="small" style="font-weight: 400;"><?php echo JText::_('COM_W7SEOUPLIFTER_SERP_DISCLAIMER'); ?></span>
    </label>
</div>
<div class="w7_serp_prev_container">
    <div class="w7_serp_domain_wrapper">
        <cite class="domain"><?php echo $domain; ?></cite>
    </div>
    <div class="w7_serp_title_wrapper">
        <h3 class="title">Test title</h3>
    </div>

    <div class="w7_serp_desc_wrapper">
        <span class="description"><span class="f">15. 5. 2021 — </span> <span class="append_desc">However interestingly, Google are still internally truncating based on 16px, but the CSS will kick in way before their ellipsis is shown due to the ...</span></span>
    </div>
</div>
</div>