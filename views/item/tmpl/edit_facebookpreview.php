<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

$urlParts = parse_url(JURI::root());
$domain = preg_replace('/^www\./', '', $urlParts['host']);

?>
<script>
    jQuery(document).ready(function() {

        var title = jQuery('#jform_facebook_og_title').val()
        jQuery('#jform_facebook_og_title_prev').html(title)

        var desc = jQuery('#jform_facebook_og_description').val()
        jQuery('#jform_facebook_og_description_preview').html(desc)

        var img = jQuery('#jform_facebook_og_image').val()
        jQuery('#jform_facebook_og_image_prev').css('background-image', 'url(<?php echo JURI::root(); ?>' + img + ')')

        jQuery('#jform_facebook_og_title').on('input', function() {
            var val = jQuery(this).val()
            jQuery('#jform_facebook_og_title_prev').html(val)
        })

        jQuery('#jform_facebook_og_description').on('input', function() {
            var val = jQuery(this).val()
            jQuery('#jform_facebook_og_description_preview').html(val)
        })

        jQuery('#jform_facebook_og_image').on('change', function() {
            var img = jQuery(this).val()
            jQuery('#jform_facebook_og_image_prev').css('background-image', 'url(<?php echo JURI::root(); ?>' + img + ')')
        })

        jQuery('#jform_facebook_og_description')
    })
    
</script>
<div>
<div class="control-label">
    <label>
        <?php echo JText::_('COM_W7SEOUPLIFTER_FACEBOOK_PREVIEW'); ?> <span class="small" style="font-weight: 400;"><?php echo JText::_('COM_W7SEOUPLIFTER_FACEBOOK_PREVIEW_DISCLAIMER'); ?></span>
    </label>
</div>
<div class="card-seo-facebook">
    <div class="card-seo-facebook__image js-preview-image" id="jform_facebook_og_image_prev" style="background-image: url(/assets/meta-tags-16a33a6a8531e519cc0936fbba0ad904e52d35f34a46c97a2c9f6f7dd7d336f2.png)"></div>
    <div class="card-seo-facebook__text">
        <span class="card-seo-facebook__link js-preview-domain"><?php echo $domain; ?></span>
        <div class="card-seo-facebook__content">
            <div style="margin-top:5px">
                <div id="jform_facebook_og_title_prev" class="card-seo-facebook__title js-preview-title">W7 Extensions —&nbsp;Preview Facebook Card</div>
            </div>
            <span id="jform_facebook_og_description_preview" class="card-seo-facebook__description js-preview-description">With Meta Tags you can edit and experiment with your content then preview how your webpage will look on Google, Facebook, Twitter and more!</span>
        </div>
    </div>
</div>
</div>