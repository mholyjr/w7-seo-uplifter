<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HelloWorlds View
 *
 * @since  0.0.1
 */
class W7SeoUplifterViewDashboard extends JViewLegacy
{
	/**
	 * Display the Items view
	 *
	 * @param   string  $tpl
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{

        $this->statsImported 	= W7SeoUplifterStatsHelper::statsImported();
        $this->statsTitles 		= W7SeoUplifterStatsHelper::statsTitles();
        $this->statsDescription = W7SeoUplifterStatsHelper::statsDescription();
        $this->statsSocial 		= W7SeoUplifterStatsHelper::statsSocial();
		$this->canDo 			= JHelperContent::getActions('com_w7seouplifter');
		$errors = empty($this->get('Errors')) ? array() : $this->get('Errors');

		if ($errors)
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		W7SeoUplifterHelper::addSubmenu('redirects');

		parent::display($tpl);

        $this->addToolBar();
        $this->setDocument();
	}

    /**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{
		JToolbarHelper::title(JText::_('COM_W7SEOUPLIFTER_MANAGER_DASHBORD'));
		if($this->canDo->get('core.create')) {
			JToolbarHelper::addNew('item.add');
			JToolbarHelper::custom('items.import', 'bi bi-plus-circle', 'icon over', 'COM_W7SEOUPLIFTER_IMPORT_BUTTON', false, false);
		}
	}

    /**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_W7SEOUPLIFTER_MANAGER_DASHBORD'));
		$document->addStylesheet('components/com_w7seouplifter/assets/css/styles.css');
        $document->addStylesheet('components/com_w7seouplifter/assets/css/w7.css');
		$document->addScript('components/com_w7seouplifter/assets/js/scripts.js');
	}

}