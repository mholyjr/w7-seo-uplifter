<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

$user		= JFactory::getUser();
$userId		= $user->get('id');
?>

<form action="index.php?option=com_w7seouplifter&view=dashboard" method="post" id="adminForm" name="adminForm">
	<div class="span2">
		<div id="j-sidebar-container">
			<?php echo JHtmlSidebar::render(); ?>
			<div class="logo_container">
                <a href="https://www.w7extensions.com" target="_blank"><img class="admin_logo" src="<?php echo JURI::root() . "administrator/components/com_w7seouplifter/assets/images/w7extensions_logo_color.svg"; ?>"></a>
				<a href="https://www.w7extensions.com" target="_blank">www.w7extensions.com</a>
			</div>
		</div>
    </div>
    <div id="j-main-container" class="span10 dashboard">
        <div class="dashboard_section">
            <?php echo $this->loadTemplate('stats'); ?>
        </div>
        <div class="dashboard_section">
            <?php echo $this->loadTemplate('menu'); ?>
        </div>
        <div class="dashboard_section last">
            <?php echo $this->loadTemplate('sitemap'); ?>
        </div>
    </div>
    <input type="hidden" name="task" value=""/>
	<?php echo JHtml::_('form.token'); ?>
</form>