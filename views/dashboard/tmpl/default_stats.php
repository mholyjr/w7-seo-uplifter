<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

?>
<div class="row-fluid">
    <div class="span3">
        <div class="stats_card <?php if($this->statsImported['check']) : ?>success<?php else : ?>danger<?php endif; ?> dark">
            <div class="row-fluid">
                <div class="span3">
                    <div class="card_icon_container">
                        <i class="bi bi-diagram-2"></i>
                    </div>
                </div>
                <div class="span9">
                    <p class="card_head"><?php echo JText::_('COM_W7SEOUPLIFTER_IMPORTED_STATS'); ?></p>
                    <p class="value"><?php echo $this->statsImported['imported']; ?>/<span class="small"><?php echo $this->statsImported['total']; ?> <?php echo JText::_('COM_W7SEOUPLIFTER_IMPORTED_STATS_IN_TOTAL'); ?></span></p>
                </div>
            </div>
        </div>
    </div>

    <div class="span3">
        <div class="stats_card <?php if($this->statsTitles['check']) : ?>success<?php else : ?>danger<?php endif; ?> dark">
            <div class="row-fluid">
                <div class="span3">
                    <div class="card_icon_container">
                    <?php if($this->statsTitles['check']) : ?>
                        <i class="bi bi-check-circle"></i>
                    <?php else : ?>
                        <i class="bi bi-exclamation-circle"></i>
                    <?php endif; ?>
                    </div>
                </div>
                <div class="span9">
                    <p class="card_head"><?php echo JText::_('COM_W7SEOUPLIFTER_MISSING_TITLES_STATS'); ?></p>
                    <p class="value"><?php echo $this->statsTitles['missing']; ?>/<span class="small"><?php echo $this->statsTitles['total']; ?> <?php echo JText::_('COM_W7SEOUPLIFTER_IMPORTED_STATS_IN_TOTAL'); ?></span></p>
                </div>
            </div>
        </div>
    </div>

    <div class="span3">
        <div class="stats_card <?php if($this->statsDescription['check']) : ?>success<?php else : ?>danger<?php endif; ?> dark">
            <div class="row-fluid">
                <div class="span3">
                    <div class="card_icon_container">
                    <?php if($this->statsDescription['check']) : ?>
                        <i class="bi bi-check-circle"></i>
                    <?php else : ?>
                        <i class="bi bi-exclamation-circle"></i>
                    <?php endif; ?>
                    </div>
                </div>
                <div class="span9">
                    <p class="card_head"><?php echo JText::_('COM_W7SEOUPLIFTER_MISSING_DESCRIPTION_STATS'); ?></p>
                    <p class="value"><?php echo $this->statsDescription['missing']; ?>/<span class="small"><?php echo $this->statsDescription['total']; ?> <?php echo JText::_('COM_W7SEOUPLIFTER_IMPORTED_STATS_IN_TOTAL'); ?></span></p>
                </div>
            </div>
        </div>
    </div>

    <div class="span3">
        <a href="https://www.w7extensions.com/support-tickets" target="_blank">
            <div class="stats_card warning dark">
                <div class="row-fluid">
                    <div class="span3">
                        <div class="card_icon_container">
                            <i class="bi bi-life-preserver"></i>
                        </div>
                    </div>
                    <div class="span9">
                        <p class="card_head"><?php echo JText::_('COM_W7SEOUPLIFTER_HAVING_TROUBLES'); ?></p>
                        <p class="value"><?php echo JText::_('COM_W7SEOUPLIFTER_SUPPORT'); ?></p>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>