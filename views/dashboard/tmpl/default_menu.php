<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

?>
<div class="row-fluid dashboard_menu">
    <div class="span3">
        <a href="<?php echo JRoute::_('index.php?option=com_w7seouplifter&view=items'); ?>" class="block_menu_link">
            <div class="block_menu_item">
                <div class='text-center'>
                    <i class="bi bi-link-45deg block_menu_icon"></i>
                    <span><?php echo JText::_('COM_W7SEOUPLIFTER_MENU_ITEMS'); ?></span>
                </div>
            </div>
        </a>
    </div>
    <div class="span3">
        <a href="<?php echo JRoute::_('index.php?option=com_w7seouplifter&view=performance'); ?>" class="block_menu_link">
            <div class="block_menu_item">
                <div class='text-center'>
                    <i class="bi bi-lightning-charge block_menu_icon"></i>
                    <span><?php echo JText::_('COM_W7SEOUPLIFTER_PERFORMANCE'); ?></span>
                </div>
            </div>
        </a>
    </div>
    <div class="span3">
        <a href="<?php echo JRoute::_('index.php?option=com_w7seouplifter&view=sitemapsources'); ?>" class="block_menu_link">
            <div class="block_menu_item">
                <div class='text-center'>
                    <i class="bi bi-diagram-2 block_menu_icon"></i>
                    <span><?php echo JText::_('COM_W7SEOUPLIFTER_SITEMAP_SOURCES'); ?></span>
                </div>
            </div>
        </a>
    </div>
    <div class="span3">
        <a href="<?php echo JRoute::_('index.php?option=com_w7seouplifter&view=redirects'); ?>" class="block_menu_link">
            <div class="block_menu_item">
                <div class='text-center'>
                    <i class="bi bi-box-arrow-in-up-right block_menu_icon"></i>
                    <span><?php echo JText::_('COM_W7SEOUPLIFTER_REDIRECTS'); ?></span>
                </div>
            </div>
        </a>
    </div>
</div>
<div class="row-fluid dashboard_menu">
    <div class="span3">
        <a href="<?php echo JRoute::_('index.php?option=com_w7seouplifter&view=htaccess'); ?>" class="block_menu_link">
            <div class="block_menu_item">
                <div class='text-center'>
                    <i class="bi bi-code-square block_menu_icon"></i>
                    <span><?php echo JText::_('COM_W7SEOUPLIFTER_HTACCESS_EDITOR'); ?></span>
                </div>
            </div>
        </a>
    </div>
    <div class="span3">
        <a href="<?php echo JRoute::_('index.php?option=com_w7seouplifter&view=robots'); ?>" class="block_menu_link">
            <div class="block_menu_item">
                <div class='text-center'>
                    <i class="bi bi-google block_menu_icon"></i>
                    <span><?php echo JText::_('COM_W7SEOUPLIFTER_ROBOTS_EDITOR'); ?></span>
                </div>
            </div>
        </a>
    </div>
    <?php if($this->canDo->get('core.admin')) : ?>
    <div class="span3">
        <a href="<?php echo JRoute::_('index.php?option=com_config&view=component&component=com_w7seouplifter'); ?>" class="block_menu_link">
            <div class="block_menu_item">
                <div class='text-center'>
                    <i class="bi bi-person-check block_menu_icon"></i>
                    <span><?php echo JText::_('COM_W7SEOUPLIFTER_PERMISSIONS'); ?></span>
                </div>
            </div>
        </a>
    </div>
    <?php endif; ?>
    <?php if($this->canDo->get('core.create')) : ?>
    <div class="span3">
        <a href="<?php echo JRoute::_('index.php?option=com_w7seouplifter&task=items.import'); ?>" class="block_menu_link color primary">
            <div class="block_menu_item">
                <div class='text-center'>
                    <i class="bi bi-plus-circle block_menu_icon"></i>
                    <span><?php echo JText::_('COM_W7SEOUPLIFTER_IMPORT_BUTTON'); ?></span>
                </div>
            </div>
        </a>
    </div>
    <?php endif; ?>
</div>