<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

$filePath = JPATH_ROOT . '/sitemap.xml';
$publicPath = JURI::root() . 'sitemap.xml';

if (!JFile::exists($filePath)) {
    $publicPath = JText::_('COM_W7SEOUPLIFTER_NO_SITEMAP');
}

?>

<div class="row-fluid sitemap_row d-flex align-items-center">
    <div class="span3 d-flex align-items-center">
        <span class="sitemap_url_label">
            <?php echo JText::_('COM_W7SEOUPLIFTER_SITEMAP_URL_LABEL'); ?>
        </span>
    </div>
    <div class="span6">
        <div class="control-group mb-0">
            <div class="controls mb-0">
                <input type="text" class="w-100-per mb-0" name="sitemap_url" id="sitemap_url" disabled value="<?php echo $publicPath; ?>">
            </div>
        </div>
    </div>
    <div class="span3 d-flex justify-content-between">
        <div class="tooltip">
            <span class="btn btn-copy" onclick="copyToClipboard('sitemap_url', 'sitemap_copy_tooltip')">
                <span class="tooltiptext" id="sitemap_copy_tooltip">Copy to clipboard</span>
                <?php echo JText::_('COM_W7SEOUPLIFTER_COPY_TO_CLIPBOARD'); ?>
            </span>
        </div>
        
        <button class="btn btn-success" onclick="Joomla.submitbutton('sitemap.generateXml');"><?php echo JText::_('COM_W7SEOUPLIFTER_GENERATE_SITEMAP'); ?></button>
    </div>
</div>