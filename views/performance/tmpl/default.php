<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', '#jform_catid', null, array('disable_search_threshold' => 0 ));
JHtml::_('formbehavior.chosen', '#jform_tags', null, array('placeholder_text_multiple' => JText::_('JGLOBAL_TYPE_OR_SELECT_SOME_TAGS')));
JHtml::_('formbehavior.chosen', 'select');
$this->configFieldsets  = array('editorConfig');

//print_r($this->item);

?>
<form action="" id="adminForm" method="post" name="adminForm" class="w7p_item_edit_form w7p_form">
    <div class="span2">
		<div id="j-sidebar-container">
			<?php echo JHtmlSidebar::render(); ?>
			<div class="logo_container">
				<a href="https://www.w7extensions.com" target="_blank"><img class="admin_logo" src="<?php echo JURI::root() . "administrator/components/com_w7seouplifter/assets/images/w7extensions_logo_color.svg"; ?>"></a>
				<a href="https://www.w7extensions.com" target="_blank">www.w7extensions.com</a>
			</div>
		</div>
    </div>
    <div id="j-main-container" class="span10 dashboard">
        <?php $enabled = JPluginHelper::isEnabled('system', 'w7seouplifteroptimizer'); ?>
		<?php if (!$enabled) : ?>
		<div class="row-fluid">
            <div class="alert alert-danger">
				<?php echo JText::_('COM_W7SEOUPLIFTER_PLUGIN_OPTIMIZER_NEEDED'); ?>
			</div>
        </div>
		<?php endif; ?>
        <div class="row-fluid">
            <div class="span12 w7p_main_card">
                <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'images')); ?>
                    <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'images', JText::_('COM_W7SEOUPLIFTER_IMAGES_LABEL')); ?>
                        <div class="form-vertical">
                            <fieldset class="adminform">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <?php 
                                            foreach($this->form->getFieldset('images_general') as $field) {
                                                echo $field->renderField();
                                            }
                                        ?>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span3">
                                        <?php 
                                            foreach($this->form->getFieldset('imagesFields') as $field) {
                                                echo $field->renderField();
                                            }
                                        ?>
                                    </div>
                                    <div class="span3">
                                        <?php 
                                            foreach($this->form->getFieldset('images_retina') as $field) {
                                                echo $field->renderField();
                                            }
                                        ?>
                                    </div>
                                    <div class="span3">
                                        <?php 
                                            foreach($this->form->getFieldset('images_lazyload') as $field) {
                                                echo $field->renderField();
                                            }
                                        ?>
                                    </div>
                                    <div class="span3">
                                        <?php 
                                            foreach($this->form->getFieldset('images_cache') as $field) {
                                                echo $field->renderField();
                                            }
                                        ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    <?php echo JHtml::_('bootstrap.endTab'); ?>

                    <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'minify', JText::_('COM_W7SEOUPLIFTER_MINIFY_LABEL')); ?>
                        <div class="form-vertical">
                            <fieldset class="adminform">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <?php 
                                            foreach($this->form->getFieldset('minify_info') as $field) {
                                                echo $field->renderField();
                                            }
                                        ?>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span4">
                                        <?php 
                                            foreach($this->form->getFieldset('minify_scripts') as $field) {
                                                echo $field->renderField();
                                            }
                                        ?>
                                    </div>
                                    <div class="span4">
                                        <?php 
                                            foreach($this->form->getFieldset('minify_styles') as $field) {
                                                echo $field->renderField();
                                            }
                                        ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    <?php echo JHtml::_('bootstrap.endTab'); ?>

                <?php echo JHtml::_('bootstrap.endTabSet'); ?>
            </div>
        </div>
    </div>
    <input type="hidden" name="jform[id]" id="id" value="1">
    <input type="hidden" name="task" value=""/>
    <?php echo JHtml::_('form.token'); ?>
</form>