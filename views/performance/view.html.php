<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Performance View
 *
 * @since  0.0.1
 */
class W7SeoUplifterViewPerformance extends JViewLegacy
{

    /**
	 * View form
	 *
	 * @var         form
	 */
	protected $form = null;

	/**
	 * Display the Item view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		$this->form 	= $this->get('Form');
		$this->item 	= $this->get('Data');
		$this->canDo 	= JHelperContent::getActions('com_w7seouplifter');
		$errors = empty($this->get('Errors')) ? array() : $this->get('Errors');

		if ($errors)
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}
		
		W7SeoUplifterHelper::addSubmenu('performance');

		$this->addToolBar();
        $this->setDocument();

		parent::display($tpl);
	}

    /**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{
		$input = JFactory::getApplication()->input;

		$input->set('hidemainmenu', false);

		$title = JText::_('COM_W7SEOUPLIFTER_MANAGER_PERFORMANCE');

		JToolbarHelper::title($title, 'performance');
		if($this->canDo->get('core.edit')) {
			JToolbarHelper::apply('performance.save');
			JToolbarHelper::custom('performance.purge', 'bi bi-folder-minus', 'icon over', 'COM_W7SEOUPLIFTER_PURGE_IMAGES_BUTTON', false, false);
		}
		
	}

    /**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$document = JFactory::getDocument();
		$title = JText::_('COM_W7SEOUPLIFTER_MANAGER_PERFORMANCE');
		$document->setTitle($title);
		$document->addStylesheet('components/com_w7seouplifter/assets/css/styles.css');
        $document->addStylesheet('components/com_w7seouplifter/assets/css/w7.css');
	}

}