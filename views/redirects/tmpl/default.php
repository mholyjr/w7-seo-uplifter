<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

JHtml::_('formbehavior.chosen', 'select');
jimport( 'joomla.application.component.helper' );

$listOrder     = $this->escape($this->filter_order);
$listDirn      = $this->escape($this->filter_order_Dir);

$user		= JFactory::getUser();
$userId		= $user->get('id');

?>

<form action="index.php?option=com_w7seouplifter&view=redirects" method="post" id="adminForm" name="adminForm">
	<div class="span2">
		<div id="j-sidebar-container">
			<?php echo JHtmlSidebar::render(); ?>
			<div class="logo_container">
				<a href="https://www.w7extensions.com" target="_blank"><img class="admin_logo" src="<?php echo JURI::root() . "administrator/components/com_w7seouplifter/assets/images/w7extensions_logo_color.svg"; ?>"></a>
				<a href="https://www.w7extensions.com" target="_blank">www.w7extensions.com</a>
			</div>
		</div>
    </div>
    <div id="j-main-container" class="span10">
		<div class="row-fluid">
			<div class="span12">
				<?php
					echo JLayoutHelper::render(
						'joomla.searchtools.default',
						array('view' => $this)
					);
				?>
			</div>
		</div>
		<?php $enabled = JPluginHelper::isEnabled('system', 'w7seouplifterredirects'); ?>
		<?php if (!$enabled) : ?>
		<div class="row-fluid">
            <div class="alert alert-danger">
				<?php echo JText::_('COM_W7SEOUPLIFTER_PLUGIN_REDIRECTS_NEEDED'); ?>
			</div>
        </div>
		<?php endif; ?>
		<div class="row-fluid">
			<div class="span12">
				<?php if (empty($this->items)) : ?>
				<div style="padding: 15px;">
					<div class="alert alert-no-items">
						<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
					</div>
				</div>
				<?php else : ?>
                <table class="table table-striped table-hover">
					<thead>
                        <tr>
                            <th width="5%">
                                <?php echo JHtml::_('grid.checkall'); ?>
                            </th>
                            <th width="35%">
                                <?php echo JText::_('COM_W7SEOUPLIFTER_FROM') ;?>
                            </th>
                            <th width="35%">
                                <?php echo JText::_('COM_W7SEOUPLIFTER_TO'); ?>
                            </th>
							<th width="12%">
                                <?php echo JText::_('COM_W7SEOUPLIFTER_REDIRECT_STATUS_LABEL'); ?>
                            </th>
                            <th width="8%">
                                <?php echo JText::_('COM_W7SEOUPLIFTER_PUBLISHED'); ?>
                            </th>
                            <th width="5%">
                                <?php echo JText::_('COM_W7SEOUPLIFTER_ID'); ?>
                            </th>
                        </tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="5">
								<?php echo $this->pagination->getListFooter(); ?>
							</td>
						</tr>
					</tfoot>
					<tbody>
						<?php if (!empty($this->items)) : ?>
							<?php foreach ($this->items as $i => $row) : 
								$link = JRoute::_('index.php?option=com_w7seouplifter&task=redirect.edit&id=' . $row->id);    
							?>
								<tr>
									<td>
										<?php echo JHtml::_('grid.id', $i, $row->id); ?>
									</td>
									<td>
										<a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_W7SEOUPLIFTER_EDIT_ITEM'); ?>">
											<?php echo $row->from; ?>
										</a>
									</td>
									<td>
										<a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_W7SEOUPLIFTER_EDIT_ITEM'); ?>">
											<?php echo $row->to; ?>
										</a>
									</td>
									<td>
										<?php echo $row->redirect_status; ?>
									</td>
									<td align="center">
										<?php echo JHtml::_('jgrid.published', $row->published, $i, 'redirects.', true, 'cb'); ?>
									</td>
									<td align="center">
										<?php echo $row->id; ?>
									</td>
								</tr>
							<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>
				<?php endif; ?>
            </div>
        </div>
    </div>
    <input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
	<?php echo JHtml::_('form.token'); ?>
</form>