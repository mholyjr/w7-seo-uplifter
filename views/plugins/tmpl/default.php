<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

$user		= JFactory::getUser();
$userId		= $user->get('id');
?>

<form action="index.php?option=com_w7seouplifter&view=plugins" method="post" id="adminForm" name="adminForm">
	<div class="span2">
		<div id="j-sidebar-container">
			<?php echo JHtmlSidebar::render(); ?>
			<div class="logo_container">
                <a href="https://www.w7extensions.com" target="_blank"><img class="admin_logo" src="<?php echo JURI::root() . "administrator/components/com_w7seouplifter/assets/images/w7extensions_logo_color.svg"; ?>"></a>
				<a href="https://www.w7extensions.com" target="_blank">www.w7extensions.com</a>
			</div>
		</div>
    </div>
    <div id="j-main-container" class="span10 dashboard">
        <div class="row-fluid">
            <div class="alert alert-info">
				<?php echo JText::_('COM_W7SEOUPLIFTER_PLUGINS_NEEDED'); ?>
			</div>
        </div>
            <?php
                $i = 0;
                foreach($this->items as $item) : 
                $enabled = JPluginHelper::isEnabled($item->folder, $item->element);
                $link = JRoute::_('index.php?option=com_w7seouplifter&task=plugins.toggle&id=' . $item->extension_id);

                if($i === 0) {
                    echo '<div class="row-fluid mb-5">';
                }

                if($i % 3 === 0 && $i !== 0) {
                    echo '</div>';
                    echo '<div class="row-fluid">';
                }
            ?>
                <div class="span4">
                    <div class="block_menu_item">
                        <div class='text-center'>
                            <?php if($enabled) : ?>
                            <i class="bi bi-check-circle-fill block_menu_icon success"></i>
                            <?php else : ?>
                            <i class="bi bi-x-circle-fill block_menu_icon danger"></i>
                            <?php endif; ?>
                            <span><?php echo $item->name; ?></span>
                            <?php if($this->canDo->get('core.edit')) : ?>
                            <a class='bubble_link <?php if($enabled) : ?>danger<?php else : ?>success<?php endif; ?>' href="<?php echo $link; ?>"><?php if($enabled) { echo JText::_('COM_W7SEOUPLIFTER_DISABLE'); } else { echo JText::_('COM_W7SEOUPLIFTER_ENABLE'); } ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php $i++; endforeach; ?>
            </div>
    </div>
    <input type="hidden" name="task" value=""/>
	<?php echo JHtml::_('form.token'); ?>
</form>