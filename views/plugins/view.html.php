<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Plugins View
 *
 * @since  0.0.1
 */
class W7SeoUplifterViewPlugins extends JViewLegacy
{
	/**
	 * Display the Plugins view
	 *
	 * @param   string  $tpl
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
        $this->items			= $this->get('Items');
		$this->canDo 			= JHelperContent::getActions('com_w7seouplifter');
        $errors = empty($this->get('Errors')) ? array() : $this->get('Errors');

		if ($errors)
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		W7SeoUplifterHelper::addSubmenu('redirects');

		parent::display($tpl);

        $this->addToolBar();
        $this->setDocument();
	}

    /**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{
		JToolbarHelper::title(JText::_('COM_W7SEOUPLIFTER_MANAGER_PLUGINS'));
	}

    /**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_W7SEOUPLIFTER_MANAGER_PLUGINS'));
		$document->addStylesheet('components/com_w7seouplifter/assets/css/styles.css');
        $document->addStylesheet('components/com_w7seouplifter/assets/css/w7.css');
	}

}