<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\Registry\Registry;

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', '#jform_catid', null, array('disable_search_threshold' => 0 ));
JHtml::_('formbehavior.chosen', '#jform_tags', null, array('placeholder_text_multiple' => JText::_('JGLOBAL_TYPE_OR_SELECT_SOME_TAGS')));
JHtml::_('formbehavior.chosen', 'select');
$this->configFieldsets  = array('editorConfig');

$listOrder     = $this->escape($this->filter_order);
$listDirn      = $this->escape($this->filter_order_Dir);

?>
<form action="index.php?option=com_w7seouplifter&view=sitemapsources" method="post" id="adminForm" name="adminForm">
    <div class="span2">
		<div id="j-sidebar-container">
			<?php echo JHtmlSidebar::render(); ?>
			<div class="logo_container">
				<a href="https://www.w7extensions.com" target="_blank"><img class="admin_logo" src="<?php echo JURI::root() . "administrator/components/com_w7seouplifter/assets/images/w7extensions_logo_color.svg"; ?>"></a>
				<a href="https://www.w7extensions.com" target="_blank">www.w7extensions.com</a>
			</div>
		</div>
    </div>
    <div id="j-main-container" class="span10">
        <div class="row-fluid">
			<div class="span12">
				<?php
					echo JLayoutHelper::render(
						'joomla.searchtools.default',
						array('view' => $this)
					);
				?>
			</div>
		</div>
        <div class="row-fluid">
				<?php if (empty($this->sources)) : ?>
				<div style="padding: 15px;">
					<div class="alert alert-no-items">
						<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
					</div>
				</div>
				<?php else : ?>
				    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="5%">
                                    <?php echo JHtml::_('grid.checkall'); ?>
                                </th>
                                <th width="35%">
                                    <?php echo JHtml::_('grid.sort', 'COM_W7SEOUPLIFTER_SITEMAP_SOURCE_TITLE', 'title', $listDirn, $listOrder); ?>
                                </th>
                                <th width="16%">
                                    <?php echo JText::_('COM_W7SEOUPLIFTER_SITEMAP_SOURCE_PRIORITY'); ?>
                                </th>
                                <th width="16%">
                                    <?php echo JText::_('COM_W7SEOUPLIFTER_SITEMAP_SOURCE_FREQUENCY'); ?>
                                </th>
                                <th width="15%">
                                    <?php echo JHtml::_('grid.sort', 'COM_W7SEOUPLIFTER_SITEMAP_SOURCE_TYPE', 'type', $listDirn, $listOrder); ?>
                                </th>
                                <th width="8%">
                                    <?php echo JHtml::_('grid.sort', 'COM_W7SEOUPLIFTER_PUBLISHED', 'published', $listDirn, $listOrder); ?>
                                </th>
                                <th width="5%">
                                    <?php echo JHtml::_('grid.sort', 'COM_W7SEOUPLIFTER_ID', 'id', $listDirn, $listOrder); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($this->sources)) : ?>
                            <?php foreach ($this->sources as $i => $row) : 
                                $link = JRoute::_('index.php?option=com_w7seouplifter&task=sitemapsource.edit&id=' . $row->id);
                                if ($row AND property_exists($row, 'params'))
                                {
                                    $registry = new Registry($row->params);
                                    $row->params = $registry->toArray();
                                }
                            ?>
                                <tr>
                                    <td>
                                        <?php echo JHtml::_('grid.id', $i, $row->id); ?>
                                    </td>
                                    <td>
                                        <a href="<?php echo $link; ?>" title="<?php echo JText::_('COM_W7SEOUPLIFTER_EDIT_ITEM'); ?>">
                                            <?php echo $row->title; ?>
                                        </a>
                                        <?php if(!empty($row->description)) : ?>
                                        <br>
                                        <span class="small">
                                            <i class="bi bi-info-circle"></i>
                                            <?php echo mb_strimwidth($row->description, 0, 120, "..."); ?>
                                        </span>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php echo $row->params['element_priority']; ?>
                                    </td>
                                    <td>
                                        <?php echo $row->params['element_frequency']; ?>
                                    </td>
                                    <td>
                                        <?php echo $row->type; ?>
                                    </td>
                                    <td align="center">
                                        <?php echo JHtml::_('jgrid.published', $row->published, $i, 'sitemapsources.', true, 'cb'); ?>
                                    </td>
                                    <td align="center">
                                        <?php echo $row->id; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
				<?php endif; ?>
			</div>
		</div>
    </div>
    <input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
	<?php echo JHtml::_('form.token'); ?>
</form>