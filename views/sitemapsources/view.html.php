<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Item View
 *
 * @since  0.0.1
 */
class W7SeoUplifterViewSitemapsources extends JViewLegacy
{
	/**
	 * View form
	 *
	 * @var         form
	 */
	protected $form = null;

	/**
	 * Display the Item view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		$app = JFactory::getApplication();
		$context = "w7seouplifter.list.admin.sitemapsource";

        $this->sources  		= $this->get('Items');
		$this->state			= $this->get('State');
		$this->filter_order 	= $app->getUserStateFromRequest($context.'filter_order', 'filter_order', 'title', 'cmd');
		$this->filter_order_Dir = $app->getUserStateFromRequest($context.'filter_order_Dir', 'filter_order_Dir', 'asc', 'cmd');
		$this->filterForm    	= $this->get('FilterForm');
		$this->activeFilters 	= $this->get('ActiveFilters');
		$this->canDo 			= JHelperContent::getActions('com_w7seouplifter');
		$errors = empty($this->get('Errors')) ? array() : $this->get('Errors');

		if ($errors)
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		W7SeoUplifterHelper::addSubmenu('performance');

		$this->addToolBar();
        $this->setDocument();

		parent::display($tpl);
	}

    /**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{
		$input = JFactory::getApplication()->input;

		$input->set('hidemainmenu', false);

		$title = JText::_('COM_W7SEOUPLIFTER_MANAGER_SITEMAP_SOURCES');

		JToolbarHelper::title(JText::_('COM_W7SEOUPLIFTER_MANAGER_SITEMAP_SOURCES'));
		if ($this->canDo->get('core.create')) {
			JToolbarHelper::addNew('sitemapsource.add');
		}

		if ($this->canDo->get('core.edit')) {
			JToolbarHelper::editList('sitemapsource.edit');	
		}

		if ($this->canDo->get('core.edit.state')) {
			JToolbarHelper::publish('sitemapsources.publish', 'JTOOLBAR_PUBLISH', true);
			JToolbarHelper::unpublish('sitemapsources.unpublish', 'JTOOLBAR_UNPUBLISH', true);
		}

		if ($this->state->get('filter.published') == -2 && $this->canDo->get('core.delete')) {
			JToolbarHelper::trash('sitemapsources.delete');
		} elseif ($this->canDo->get('core.edit.state')) {
			JToolbarHelper::trash('sitemapsources.trash');
		}

		if($this->canDo->get('core.edit')) {
			JToolbarHelper::custom('sitemap.generateXml', 'bi bi-folder-plus', 'icon over', 'COM_W7SEOUPLIFTER_GENERATE_SITEMAP', false, false);
		}
		
	}

    /**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$document = JFactory::getDocument();
		$title = JText::_('COM_W7SEOUPLIFTER_MANAGER_SITEMAP_SOURCES');
		$document->setTitle($title);
		$document->addStylesheet('components/com_w7seouplifter/assets/css/styles.css');
        $document->addStylesheet('components/com_w7seouplifter/assets/css/w7.css');
	}

}