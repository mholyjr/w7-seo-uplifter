<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.formvalidator');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', '#jform_catid', null, array('disable_search_threshold' => 0 ));
JHtml::_('formbehavior.chosen', '#jform_tags', null, array('placeholder_text_multiple' => JText::_('JGLOBAL_TYPE_OR_SELECT_SOME_TAGS')));
JHtml::_('formbehavior.chosen', 'select');
$this->configFieldsets  = array('editorConfig');

?>
<form class="w7p_item_edit_form w7p_form" action="<?php echo JRoute::_('index.php?option=com_w7seouplifter&view=redirect&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row-fluid">
        <div class="span9 w7p_main_card">
            <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'meta')); ?>
                <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'meta', JText::_('COM_W7SEOUPLIFTER_REDIRECT_LABEL')); ?>
                    <div class="form-vertical">
                        <fieldset class="adminform">
                            <div class="row-fluid">
                                <div class="span4">
                                    <?php 
                                        foreach($this->form->getFieldset('basic') as $field) {
                                            if($field->name == 'jform[from]') {
                                                echo $field->renderField();
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="span4">
                                    <?php 
                                        foreach($this->form->getFieldset('basic') as $field) {
                                            if($field->name == 'jform[to]') {
                                                echo $field->renderField();
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="span4" style="border-left: 1px solid #ececec; padding-left: 30px;">
                                    <?php 
                                        foreach($this->form->getFieldset('basic') as $field) {
                                            if($field->name == 'jform[redirect_status]') {
                                                echo $field->renderField();
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                <?php echo JHtml::_('bootstrap.endTab'); ?>
            <?php echo JHtml::_('bootstrap.endTabSet'); ?>
        </div>
        <div class="span3 w7p_edit_sidebar">
            <div class="w7p_card">
                <div class="w7p_card_title">
                    <h3><?php echo JText::_('COM_W7SEOUPLIFTER_ADDITIONAL_LABEL'); ?></h3>
                </div>
                <fieldset class="adminform">
                    <div class="row-fluid">
                        <div class="span12">
                            <?php 
                                foreach($this->form->getFieldset('additional') as $field) {
                                    echo $field->renderField();
                                }
                            ?>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
    <input type="hidden" name="task" value="redirect.edit" />
    <?php echo JHtml::_('form.token'); ?>
</form>