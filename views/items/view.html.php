<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HelloWorlds View
 *
 * @since  0.0.1
 */
class W7SeoUplifterViewItems extends JViewLegacy
{
	/**
	 * Display the Items view
	 *
	 * @param   string  $tpl
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{

		$app = JFactory::getApplication();
		$context = "w7seouplifter.list.admin.item";

		$this->items			= $this->get('Items');
		$this->pagination		= $this->get('Pagination');
		$this->state			= $this->get('State');
		$this->filter_order 	= $app->getUserStateFromRequest($context.'filter_order', 'filter_order', 'title', 'cmd');
		$this->filter_order_Dir = $app->getUserStateFromRequest($context.'filter_order_Dir', 'filter_order_Dir', 'asc', 'cmd');
		$this->filterForm    	= $this->get('FilterForm');
		$this->activeFilters 	= $this->get('ActiveFilters');
		$this->canDo 			= JHelperContent::getActions('com_w7seouplifter');
		$errors = empty($this->get('Errors')) ? array() : $this->get('Errors');

		if ($errors)
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		W7SeoUplifterHelper::addSubmenu('redirects');

		parent::display($tpl);

        $this->addToolBar();
        $this->setDocument();
	}

    /**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{
		JToolbarHelper::title(JText::_('COM_W7SEOUPLIFTER_MANAGER_ITEMS'));
		if ($this->canDo->get('core.create')) {
			JToolbarHelper::addNew('item.add');
		}

		if ($this->canDo->get('core.edit')) {
			JToolbarHelper::editList('item.edit');
		}

		if ($this->canDo->get('core.edit.state')) {
			JToolbarHelper::publish('items.publish', 'JTOOLBAR_PUBLISH', true);
			JToolbarHelper::unpublish('items.unpublish', 'JTOOLBAR_UNPUBLISH', true);
		}

		if ($this->state->get('filter.published') == -2 && $this->canDo->get('core.delete')) {
			JToolbarHelper::trash('items.delete');
		} elseif ($this->canDo->get('core.edit.state')) {
			JToolbarHelper::trash('items.trash');
		}

		if ($this->canDo->get('core.create')) {
			JToolbarHelper::custom('items.import', 'bi bi-plus-circle', 'icon over', 'COM_W7SEOUPLIFTER_IMPORT_BUTTON', false, false);
		}
	}

    /**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_W7SEOUPLIFTER_MANAGER_ITEMS'));
		$document->addStylesheet('components/com_w7seouplifter/assets/css/styles.css');
        $document->addStylesheet('components/com_w7seouplifter/assets/css/w7.css');
	}
}