<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Access check
if (!JFactory::getUser()->authorise('core.manage', 'com_w7seouplifter')) 
{
    throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

$controller = JControllerLegacy::getInstance('W7SeoUplifter');

JLoader::register('W7SeoUplifterHelper', JPATH_COMPONENT . '/helpers/w7seouplifter.php');
JLoader::register('W7SeoUplifterStatsHelper', JPATH_COMPONENT . '/helpers/stats.php');

$controller->execute(JFactory::getApplication()->input->get('task'));

$controller->redirect();