<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Hello Table class
 *
 * @since  0.0.1
 */
class W7SeoUplifterTableW7SeoUplifter extends JTable
{
	/**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct('#__w7seouplifter_items', 'id', $db);
	}

	/**
	 * Overloaded bind function
	 *
	 * @param       array           named array
	 * @return      null|string     null is operation was satisfactory, otherwise returns an error
	 * @see JTable:bind
	 * @since 1.5
	 */
	public function bind($array, $ignore = '')
	{
		if (isset($array['facebook']) && is_array($array['facebook']))
		{
			$parameter = new JRegistry;
			$parameter->loadArray($array['facebook']);
			$array['facebook'] = (string)$parameter;
		}

		if (isset($array['twitter']) && is_array($array['twitter']))
		{
			$parameter = new JRegistry;
			$parameter->loadArray($array['twitter']);
			$array['twitter'] = (string)$parameter;
		}

		return parent::bind($array, $ignore);
	}
	
}