<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Plugins Model
 *
 * @since  0.0.1
 */
class W7SeoUplifterModelPlugins extends JModelList
{
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('i.extension_id, i.type, i.element, i.folder, i.name')
                ->from($db->quoteName('#__extensions', 'i'))
                ->where('i.element LIKE "w7seouplifter%"')
                ->where('i.type = "plugin"');

		return $query;
	}

    /**
     * Method to toggle plugin's state
     * 
     * @param   int     $id     ID of the plugin to toggle
     */
    public function togglePlugin($id)
    {
        $db    = JFactory::getDbo();
		$query = $db->getQuery(true);

        $query->select('i.enabled')
                ->from($db->quoteName('#__extensions', 'i'))
                ->where($db->quoteName('i.extension_id') . ' = ' . $id);
        $db->setQuery((string)$query);
        $prevState = $db->loadResult();

        if($prevState == 1) {
            $newState = 0;
        } else if($prevState == 0) {
            $newState = 1;
        } else {
            return;
        }

        $query = $db->getQuery(true);

        $fields = array(
            $db->quoteName('enabled') . ' = ' . $newState,
        );

        $conditions = array(
            $db->quoteName('extension_id') . ' = ' . $id
        );

        $query->update($db->quoteName('#__extensions'))->set($fields)->where($conditions);

        $db->setQuery($query);
        $db->execute();

        return $newState;
    }
}