<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\Registry\Registry;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Date\Date;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;

jimport('joomla.application.component.controller');

JLoader::register('ContentHelperRoute', JPATH_SITE . '/components/com_content/helpers/route.php');
JLoader::register('TagsHelperRoute', JPATH_SITE . '/components/com_tags/helpers/route.php');
JLoader::register('DJClassifiedsSEO', JPATH_ADMINISTRATOR.'/components/com_djclassifieds/lib/djseo.php');
JLoader::register('K2HelperRoute', JPATH_SITE.'/components/com_k2/helpers/route.php');

/**
 * Sitemap Model
 *
 * @since  0.0.1
 */
class W7SeoUplifterModelSitemap extends JModelAdmin
{

    /**
     * Method to create the xml sitemap file
     * 
     * @return  boolean
     */
    public function generateSitemap()
    {
        $xml = new SimpleXMLElement('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"/>');

        // All the links for the sitemap
        $links = $this->getSitemapData();
        $used = array();
        $lastmod = new Date();

        if(empty($links)) {
            \JLog::add(\JText::_('COM_W7SEOUPLIFTER_SITEMAP_NO_SOURCES'), \JLog::ERROR, 'jerror');
    		return false;
        }

        foreach($links as $item) {
            $route = Route::link('site', $item['link'], true, 0, true);

            // If not already in the sitemap
            if(!in_array($route, $used)) {
                $url = $xml->addChild('url');
                $url->addChild('loc', $route);
                $url->addChild('changefreq', $item['frequency']);
                $url->addChild('priority', ($item['priority'] / 10));
                $url->addChild('lastmod', HtmlHelper::date($lastmod, Text::_('DATE_FORMAT_LC4')));

                $used[] = $route;
            }
        }

        $filePath = JPATH_ROOT . '/sitemap.xml';

        if (JFile::exists($filePath)) {
            JFile::delete($filePath);
        }

        //Format XML to save indented tree rather than one line
        $dom = new DOMDocument('1.0');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($xml->asXML());

        //Save XML to file - remove this and following line if save not desired
        $dom->save($filePath);

        JFile::append($filePath, $xml);

        return true;
    }

    /**
     * Method to get sitemap data
     * 
     * @return  array
     */
    public function getSitemapData()
    {
        $db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('i.*')
                ->from($db->quoteName('#__w7seouplifter_sitemap_sources', 'i'))
                ->where($db->quoteName('i.published') . ' = ' . 1);

        $db->setQuery((string)$query);
        $items = $db->loadObjectList();

        // All links for the sitemap
        $links = array();
        
        foreach($items as $item) {
            if ($item AND property_exists($item, 'params'))
            {
                $registry = new Registry($item->params);
                $item->params = $registry->toArray();
            }
        }
                
        foreach($items as $item) {
            switch($item->type) {
                case 'joomlaarticles':
                    $data = $this->getArticlesData($item->params);
                    $links = array_merge($links, $data);
                break;
                case 'menu':
                    $data = $this->getMenuData($item->params);
                    $links = array_merge($links, $data);
                break;
                case 'joomlacategories':
                    $data = $this->getCategoriesData($item->params);
                    $links = array_merge($links, $data);
                break;
                case 'joomlatags':
                    $data = $this->getJoomlaTagsData($item->params);
                    $links = array_merge($links, $data);
                break;
                case 'djclassifiedsitems':
                    if (JComponentHelper::isEnabled('com_djclassifieds', true)) {
                        $data = $this->getDjClassifiedsItemsData($item->params);
                        $links = array_merge($links, $data);
                    }
                break;
                case 'djclassifiedscategories':
                    if (JComponentHelper::isEnabled('com_djclassifieds', true)) {
                        $data = $this->getDjCategoriesData($item->params);
                        $links = array_merge($links, $data);
                    }
                break;
                case 'djclassifiedsprofiles':
                    if (JComponentHelper::isEnabled('com_djclassifieds', true)) {
                        $data = $this->getDjProfilesData($item->params);
                        $links = array_merge($links, $data);
                    }
                break;
                case 'k2articles':
                    if (JComponentHelper::isEnabled('com_k2', true)) {
                        $data = $this->getK2ArticlesData($item->params);
                        $links = array_merge($links, $data);
                    }
                break;
                case 'k2categories':
                    if (JComponentHelper::isEnabled('com_k2', true)) {
                        $data = $this->getK2CategoriesData($item->params);
                        $links = array_merge($links, $data);
                    }
                break;
            }
        }

        return $links;
    }

    /**
	 * Method to build an SQL query to load the menu data.
     * 
     * @param   array     $params - Menutype of the selected menu
	 *
	 * @return  array
	 */
	private function getMenuData(array $params)
	{

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
        $exclude = '';

        if(!empty($params['exclude_menuitem'])) {
            $exclude = implode(",", $params['exclude_menuitem']);
        }

		$query->select('m.link')
                ->from($db->quoteName('#__menu', 'm'))
                ->where($db->quoteName('m.client_id') . ' = ' . 0)
                ->where($db->quoteName('m.access') . ' = ' . 1)
                ->where($db->quoteName('m.published') . ' = ' . 1)
                ->where($db->quoteName('m.menutype') . ' = "' . $params['menutype'] . '"');

        if(!empty($exclude)) {
            $query->where($db->quoteName('m.id') . ' NOT IN (' . $exclude . ')');
        }

        $db->setQuery($query);
        $column = $db->loadColumn();

        $data = array();
        $i = 0;
        foreach($column as $item) {
            $data[$i]['link'] = $item;
            $data[$i]['priority'] = $params['element_priority'];
            $data[$i]['frequency'] = $params['element_frequency'];

            $i++;
        }

		return $data;
	}

    /**
	 * Method to build an SQL query to load the Joomla articles data.
     * 
     * @param   array     $params - content params
	 *
	 * @return  array
	 */
	private function getArticlesData(array $params)
	{

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
        $exclude = '';

        if(!empty($params['exclude_category'])) {
            $exclude = implode(",", $params['exclude_category']);
        }
        
		$query->select('i.id, i.catid')
                ->from($db->quoteName('#__content', 'i'));

        if(!empty($exclude)) {
            $query->where($db->quoteName('i.catid') . ' NOT IN (' . $exclude . ')');
        }
                
        $query->where($db->quoteName('i.state') . ' = ' . 1);

        $db->setQuery($query);
        $column = $db->loadObjectList();

        $data = array();
        $i = 0;

        foreach($column as $item) {
            $link = ContentHelperRoute::getArticleRoute($item->id, $item->catid);
            $data[$i]['link'] = $link;
            $data[$i]['priority'] = $params['element_priority'];
            $data[$i]['frequency'] = $params['element_frequency'];

            $i++;
        }

		return $data;
	}

    /**
	 * Method to build an SQL query to load the Joomla categories data.
     * 
     * @param   array     $params - content params
	 *
	 * @return  array
	 */
	private function getCategoriesData(array $params)
	{

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
        $exclude = '';

        if(!empty($params['exclude_category'])) {
            $exclude = implode(",", $params['exclude_category']);
        }
        
		$query->select('i.id')
                ->from($db->quoteName('#__categories', 'i'));

        if(!empty($exclude)) {
            $query->where($db->quoteName('i.id') . ' NOT IN (' . $exclude . ')');
        }
                
        $query->where($db->quoteName('i.published') . ' = ' . 1);
        $query->where($db->quoteName('i.extension') . ' = "com_content"');

        $db->setQuery($query);
        $column = $db->loadObjectList();

        $data = array();
        $i = 0;

        foreach($column as $item) {
            $link = ContentHelperRoute::getCategoryRoute($item->id);
            $data[$i]['link'] = $link;
            $data[$i]['priority'] = $params['element_priority'];
            $data[$i]['frequency'] = $params['element_frequency'];

            $i++;
        }

		return $data;
	}

    /**
	 * Method to build an SQL query to load the Joomla tags data.
     * 
     * @param   array     $params - content params
	 *
	 * @return  array
	 */
	private function getJoomlaTagsData(array $params)
	{

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
        $exclude = '';

        if(!empty($params['exclude_tag'])) {
            $exclude = implode(",", $params['exclude_tag']);
        }
        
		$query->select('i.id')
                ->from($db->quoteName('#__tags', 'i'));

        if(!empty($exclude)) {
            $query->where($db->quoteName('i.id') . ' NOT IN (' . $exclude . ')');
        }
                
        $query->where($db->quoteName('i.published') . ' = ' . 1);
        $query->where($db->quoteName('i.access') . ' = ' . 1);

        $db->setQuery($query);
        $column = $db->loadObjectList();

        $data = array();
        $i = 0;

        foreach($column as $item) {
            $link = TagsHelperRoute::getTagRoute($item->id);
            $data[$i]['link'] = $link;
            $data[$i]['priority'] = $params['element_priority'];
            $data[$i]['frequency'] = $params['element_frequency'];

            $i++;
        }

		return $data;
	}

    /**
	 * Method to build an SQL query to load the DJ Classifieds items data.
     * 
     * @param   array     $params - content params
	 *
	 * @return  array
	 */
    public function getDjClassifiedsItemsData($params)
    {
        $db    = JFactory::getDbo();
		$query = $db->getQuery(true);
        $exclude = '';
        $exclude_cat = '';
        $date = new Date();
        $today = HtmlHelper::date($date, Text::_('DATE_FORMAT_LC4'));

        if(!empty($params['exclude_dj_item'])) {
            $exclude = implode(",", $params['exclude_dj_item']);
        }

        if(!empty($params['exclude_dj_category'])) {
            $exclude_cat = implode(",", $params['exclude_dj_category']);
        }
        
		$query->select('i.id, i.alias, i.cat_id, c.alias AS c_alias, i.region_id, r.name AS r_name')
                ->from($db->quoteName('#__djcf_items', 'i'));

        $query->join('LEFT', $db->quoteName('#__djcf_categories', 'c') . ' ON ' . $db->quoteName('i.cat_id') . ' = ' . $db->quoteName('c.id'));
        $query->join('LEFT', $db->quoteName('#__djcf_regions', 'r') . ' ON ' . $db->quoteName('i.region_id') . ' = ' . $db->quoteName('r.id'));

        if(!empty($exclude)) {
            $query->where($db->quoteName('i.id') . ' NOT IN (' . $exclude . ')');
        }

        if(!empty($exclude_cat)) {
            $query->where($db->quoteName('i.cat_id') . ' NOT IN (' . $exclude_cat . ')');
        }
                
        $query->where($db->quoteName('i.published') . ' = ' . 1);
        $query->where($db->quoteName('i.date_exp') . ' > "' . $today . '"');

        $db->setQuery($query);
        $column = $db->loadObjectList();

        $data = array();
        $i = 0;

        foreach($column as $item) {
            $link = DJClassifiedsSEO::getItemRoute($item->id.':'.$item->alias,$item->cat_id.':'.$item->c_alias,$item->region_id.':'.$item->r_name);
            $data[$i]['link'] = $link;
            $data[$i]['priority'] = $params['element_priority'];
            $data[$i]['frequency'] = $params['element_frequency'];

            $i++;
        }

		return $data;
    }

    /**
	 * Method to build an SQL query to load the Joomla categories data.
     * 
     * @param   array     $params - content params
	 *
	 * @return  array
	 */
	private function getDjCategoriesData(array $params)
	{

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
        $exclude = '';

        if(!empty($params['exclude_category'])) {
            $exclude = implode(",", $params['exclude_category']);
        }
        
		$query->select('i.id')
                ->from($db->quoteName('#__djcf_categories', 'i'));

        if(!empty($exclude)) {
            $query->where($db->quoteName('i.id') . ' NOT IN (' . $exclude . ')');
        }
                
        $query->where($db->quoteName('i.published') . ' = ' . 1);

        $db->setQuery($query);
        $column = $db->loadObjectList();

        $data = array();
        $i = 0;

        foreach($column as $item) {
            $link = DJClassifiedsSEO::getCategoryRoute($item->id);
            $data[$i]['link'] = $link;
            $data[$i]['priority'] = $params['element_priority'];
            $data[$i]['frequency'] = $params['element_frequency'];

            $i++;
        }

		return $data;
	}

    /**
	 * Method to build an SQL query to load the DJ Classifieds profiles data.
     * 
     * @param   array     $params - content params
	 *
	 * @return  array
	 */
	private function getDjProfilesData(array $params)
	{

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
        $exclude = '';

        if(!empty($params['exclude_user'])) {
            $exclude = implode(",", $params['exclude_user']);
        }
        
		$query->select('i.id', 'i.username')
                ->from($db->quoteName('#__users', 'i'));

        if(!empty($exclude)) {
            $query->where($db->quoteName('i.id') . ' NOT IN (' . $exclude . ')');
        }
                
        $query->where($db->quoteName('i.block') . ' = ' . 0);

        $db->setQuery($query);
        $column = $db->loadObjectList();

        $data = array();
        $i = 0;

        foreach($column as $item) {
            $uid_slug = $item->id.':'.DJClassifiedsSEO::getAliasName($item->username);
            $link = 'index.php?option=com_djclassifieds&view=profile&uid=' . $uid_slug;
            $data[$i]['link'] = $link;
            $data[$i]['priority'] = $params['element_priority'];
            $data[$i]['frequency'] = $params['element_frequency'];

            $i++;
        }

		return $data;
	}

    /**
	 * Method to build an SQL query to load the K2 articles data.
     * 
     * @param   array     $params - content params
	 *
	 * @return  array
	 */
	private function getK2ArticlesData(array $params)
	{

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
        $exclude = '';

        if(!empty($params['exclude_k2_category'])) {
            $exclude = implode(",", $params['exclude_k2_category']);
        }
        
		$query->select('i.id, i.catid')
                ->from($db->quoteName('#__k2_items', 'i'));

        if(!empty($exclude)) {
            $query->where($db->quoteName('i.catid') . ' NOT IN (' . $exclude . ')');
        }
                
        $query->where($db->quoteName('i.published') . ' = ' . 1);

        $db->setQuery($query);
        $column = $db->loadObjectList();

        $data = array();
        $i = 0;

        foreach($column as $item) {
            $link = K2HelperRoute::getItemRoute($item->id, $item->catid);
            $data[$i]['link'] = $link;
            $data[$i]['priority'] = $params['element_priority'];
            $data[$i]['frequency'] = $params['element_frequency'];

            $i++;
        }

		return $data;
	}

    /**
	 * Method to build an SQL query to load the Joomla categories data.
     * 
     * @param   array     $params - content params
	 *
	 * @return  array
	 */
	private function getK2CategoriesData(array $params)
	{

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
        $exclude = '';

        if(!empty($params['exclude_k2_category'])) {
            $exclude = implode(",", $params['exclude_k2_category']);
        }
        
		$query->select('i.id')
                ->from($db->quoteName('#__k2_categories', 'i'));

        if(!empty($exclude)) {
            $query->where($db->quoteName('i.id') . ' NOT IN (' . $exclude . ')');
        }
                
        $query->where($db->quoteName('i.published') . ' = ' . 1);
        $query->where($db->quoteName('i.access') . ' = ' . 1);

        $db->setQuery($query);
        $column = $db->loadObjectList();

        $data = array();
        $i = 0;

        foreach($column as $item) {
            $link = K2HelperRoute::getCategoryRoute($item->id);
            $data[$i]['link'] = $link;
            $data[$i]['priority'] = $params['element_priority'];
            $data[$i]['frequency'] = $params['element_frequency'];

            $i++;
        }

		return $data;
	}

    /**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{}

}