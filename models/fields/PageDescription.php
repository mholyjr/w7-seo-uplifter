<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldPageDescription extends JFormField {

	protected $type = 'PageDescription';

	public function getLabel() {
		return parent::getLabel();
	}

	public function getInput() {

        $js = '<script>' .
        'jQuery(document).ready(function() {
            countChars(\''.$this->id.'\', \'w7_input_length'.$this->id.'\')
        });'.
        'function countChars(countfrom, displayto) {
            var element = document.getElementById(displayto);
            var len = document.getElementById(countfrom).value.length;
            element.innerHTML = len;

            if(len < 120) {
                element.classList.add("danger");
                element.classList.remove("warning", "success");
            } else if(len > 120 && len < 150) {
                element.classList.add("warning");
                element.classList.remove("danger", "success");
            } else if(len > 150 && len < 160) {
                element.classList.add("success");
                element.classList.remove("danger", "warning");
            } else if(len > 160 && len < 165) {
                element.classList.add("warning");
                element.classList.remove("danger", "success");
            } else if(len > 165) {
                element.classList.add("danger");
                element.classList.remove("warning", "success");
            }
          }' .
        '</script>';

		return  $js . '<div>' .
                    '<div class="w7_input_wrapper">' .
                    '<textarea id="'.$this->id.'" name="'.$this->name.'" value="'.$this->value.'" placeholder="'.$this->hint.'" type="text" onkeyup="countChars(\''.$this->id.'\', \'w7_input_length'.$this->id.'\')">'.$this->value.'</textarea>' .
                    '<div class="w7_input_length" id="w7_input_length'.$this->id.'"></div>' .
                    '</div>' .
                    '<div class="w7_input_desc_container"><span class="w7_input_desc">'. JText::_('COM_W7SEOUPLIFTER_PAGE_DESCRIPTION_INPUT_DESC') .'</span></div>' .
                '</div>';
	}
}