<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JFormHelper::loadFieldClass('list');

/**
 * HelloWorld Form Field class for the HelloWorld component
 *
 * @since  0.0.1
 */
class JFormFieldCustomUser extends JFormFieldList
{
	/**
	 * The field type.
	 *
	 * @var         string
	 */
	protected $type = 'CustomUser';

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return  array  An array of JHtml options.
	 */
	protected function getOptions()
	{
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('id, username, email');
		$query->from('#__users');
		$db->setQuery((string) $query);
		$fields = $db->loadObjectList();
		$options  = array();

		if ($fields)
		{
			foreach ($fields as $field)
			{
				$options[] = JHtml::_('select.option', $field->id, JText::_($field->username . ' (' . $field->email . ')'));
			}
		}

		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}