<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JFormHelper::loadFieldClass('list');

/**
 * HelloWorld Form Field class for the HelloWorld component
 *
 * @since  0.0.1
 */
class JFormFieldCustomCategories extends JFormFieldList
{
	/**
	 * The field type.
	 *
	 * @var         string
	 */
	protected $type = 'CustomCategories';

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return  array  An array of JHtml options.
	 */
	protected function getOptions()
	{
		$cat_ext = $this->getAttribute('extension');
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);

		switch($cat_ext) {
			case 'com_djclassifieds':
				$query->select('id, name');
				$query->from('#__djcf_categories');
			break;
			case 'com_k2':
				$query->select('id, name');
				$query->from('#__k2_categories');
			break;
		}
		

		$db->setQuery((string) $query);
		$types = $db->loadObjectList();
		$options  = array();

		if ($types)
		{
			foreach ($types as $type)
			{
				$options[] = JHtml::_('select.option', $type->id, JText::_($type->name));
			}
		}

		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}