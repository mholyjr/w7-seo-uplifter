<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldPageTitle extends JFormField {

	protected $type = 'PageTitle';

	public function getLabel() {
		return parent::getLabel();
	}

	public function getInput() {

        $js = '<script>' .
        'jQuery(document).ready(function() {
            countCharsTitle'.$this->id.'(\''.$this->id.'\', \'w7_input_length'.$this->id.'\')
        });'.
        'function countCharsTitle'.$this->id.'(countfrom, displayto) {
            var element = document.getElementById(displayto);
            var len = document.getElementById(countfrom).value.length;
            element.innerHTML = len;

            if(len < '.$this->element['min'].') {
                element.classList.add("danger");
                element.classList.remove("warning", "success");
            } else if(len >= '.$this->element['min'].' && len < '.$this->element['goodbottom'].') {
                element.classList.add("warning");
                element.classList.remove("danger", "success");
            } else if(len >= '.$this->element['goodbottom'].' && len < '.$this->element['goodtop'].') {
                element.classList.add("success");
                element.classList.remove("danger", "warning");
            } else if(len >= '.$this->element['goodtop'].' && len < '.$this->element['max'].') {
                element.classList.add("warning");
                element.classList.remove("danger", "success");
            } else if(len >= '.$this->element['max'].') {
                element.classList.add("danger");
                element.classList.remove("warning", "success");
            }
          }' .
        '</script>';

		return  $js . '<div>' .
                    '<div class="w7_input_wrapper">' .
                    '<input id="'.$this->id.'" name="'.$this->name.'" value="'.$this->value.'" placeholder="'.$this->hint.'" type="text" onkeyup="countCharsTitle'.$this->id.'(\''.$this->id.'\', \'w7_input_length'.$this->id.'\')">' .
                    '<div class="w7_input_length" id="w7_input_length'.$this->id.'"></div>' .
                    '</div>' .
                    '<div class="w7_input_desc_container"><span class="w7_input_desc">'. JText::_('COM_W7SEOUPLIFTER_PAGE_TITLE_INPUT_DESC') .'</span></div>' .
                '</div>';
	}
}