<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldPageKeywords extends JFormField {

	protected $type = 'PageKeywords';

	public function getLabel() {
		return parent::getLabel();
	}

	public function getInput() {

		return      '<div>' .
                    '<div class="w7_input_wrapper">' .
                    '<textarea id="'.$this->id.'" name="'.$this->name.'" value="'.$this->value.'" placeholder="'.$this->hint.'" type="text" onkeyup="countChars(\''.$this->id.'\', \'w7_input_length'.$this->id.'\')">'.$this->value.'</textarea>' .
                    '</div>' .
                    '<div class="w7_input_desc_container"><span class="w7_input_desc">'. JText::_('COM_W7SEOUPLIFTER_PAGE_KEYWORDS_INPUT_DESC') .'</span></div>' .
                '</div>';
	}
}