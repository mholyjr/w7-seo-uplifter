<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Redirects Model
 *
 * @since  0.0.1
 */
class W7SeoUplifterModelRedirects extends JModelList
{

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array()) {

		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id','i.id',
				'redirect_status','i.redirect_status'
			);
		}

		parent::__construct($config);
	}

	protected function populateState($ordering = 'i.id', $direction = 'desc') {
		$app = JFactory::getApplication();
		$context = $this->context;

		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);

		$redirect_status = $this->getUserStateFromRequest($this->context . '.filter.redirect_status', 'filter_published', '');
		$this->setState('filter.redirect_status', $redirect_status);

		// List state information.
		parent::populateState($ordering, $direction);
	}

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('i.*')
                ->from($db->quoteName('#__w7seouplifter_redirects', 'i'));

		// Filter: like / search
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			$like = $db->quote('%' . $search . '%');
			$query->where($db->quoteName('m.from') . ' LIKE ' . $like . ' OR ' . $db->quoteName('m.to') . ' LIKE ' . $like);
		}

		// Filter by published state
		$published = $this->getState('filter.published');

		if (is_numeric($published))
		{
			$query->where('i.published = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(i.published IN (0, 1))');
		}

		// Filter by redirect status
		$redirect_status = $this->getState('filter.redirect_status');

		if (is_numeric($redirect_status))
		{
			$query->where('i.redirect_status = ' . (int) $redirect_status);
		}

		// Add the list ordering clause.
		$orderCol	= $this->state->get('list.ordering', 'id');
		$orderDirn 	= $this->state->get('list.direction', 'asc');

		$query->order($db->escape($orderCol) . ' ' . $db->escape($orderDirn));

		return $query;
	}
}