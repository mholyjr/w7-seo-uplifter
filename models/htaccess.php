<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\Registry\Registry;
use Joomla\Utilities\ArrayHelper; 

/**
 * Htaccess Model
 *
 * @since  0.0.1
 */
class W7SeoUplifterModelHtaccess extends JModelAdmin
{

    /**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{

		$access = JHelperContent::getActions('com_w7seouplifter');
		// Get the form.
		$form = $this->loadForm(
			'com_w7seouplifter.htaccess',
			'htaccess',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form)) {
			return false;
		}

		return $form;
	}

    /**
	 * Method to get the htaccess content.
	 *
	 * This method will load the global configuration data straight from
	 * JConfig. If configuration data has been saved in the session, that
	 * data will be merged into the original data, overwriting it.
	 *
	 * @return	object  An array containing all global config data.
	 *
	 * @since	1.6
	 */
	public function getData()
	{

        $item = new \stdClass;

        $filePath = JPATH_ROOT . '/.htaccess';

        if (!JFile::exists($filePath)) {
            $filePath = JPATH_ROOT . '/htaccess.txt';
        }

        if (!JFile::exists($filePath)) {
            \JLog::add(\JText::_('COM_W7SEOUPLIFTER_NO_HTACCESS_FILE'), \JLog::WARNING, 'jerror');
    		return;
        }

        $htaccess = file_get_contents($filePath);
        $item->htaccess = $htaccess;

		return $item;
	}

    /**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState(
			'com_w7seouplifter.htaccess.data',
			array()
		);

		if (empty($data))
		{
			$data = $this->getData();
		}

		return $data;
	}

    /**
	 * Method to override the JModelAdmin save() function to handle Save as Copy correctly
	 *
	 * @param   array The W7 SEO Uplifter redirect record data submitted from the form.
	 *
	 * @return  boolean
	 */
	public function save($data)
	{
		$input  = JFactory::getApplication()->input;
		$filter = JFilterInput::getInstance();
		
		$filePath = JPATH_ROOT . '/.htaccess';

        if (JFile::exists($filePath)) {
            JFile::delete($filePath);
        }

        JFile::append($filePath, $data['htaccess']);

        return true;
	}

    /**
	 * Method to override the JModelAdmin save() function to handle Save as Copy correctly
	 *
	 * @param   array The W7 SEO Uplifter redirect record data submitted from the form.
	 *
	 * @return  boolean
	 */
	public function restore()
	{
		$filePath = JPATH_ROOT . '/.htaccess';
        $new_content = JPATH_ROOT . '/htaccess.txt';

        if (JFile::exists($filePath)) {
            JFile::delete($filePath);
        }

        if (!JFile::exists($new_content)) {
            \JLog::add(\JText::_('COM_W7SEOUPLIFTER_HTACCESS_MISSING_DEFAULT_FILE'), \JLog::WARNING, 'jerror');
    		return;
        }

        $restore = file_get_contents($new_content);

        JFile::append($filePath, $restore);

        return true;
	}

    /**
	 * Method to override the JModelAdmin save() function to handle Save as Copy correctly
	 *
	 * @param   array The W7 SEO Uplifter redirect record data submitted from the form.
	 *
	 * @return  boolean
	 */
	public function deleteFile()
	{
		$filePath = JPATH_ROOT . '/.htaccess';
        $new_content = JPATH_ROOT . '/htaccess.txt';

        if (!JFile::exists($filePath)) {
            \JLog::add(\JText::_('COM_W7SEOUPLIFTER_NO_HTACCESS_FILE_TO_DELETE'), \JLog::ERROR, 'jerror');
            return false;
        }

        JFile::delete($filePath);

        return true;
	}
}