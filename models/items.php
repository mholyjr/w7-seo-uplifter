<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Items Model
 *
 * @since  0.0.1
 */
class W7SeoUplifterModelItems extends JModelList
{

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array()) {

		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id','i.id',
				'title','i.title',
				'id_menu', 'i.id_menu'
			);
		}

		parent::__construct($config);
	}

	protected function populateState($ordering = 'i.id', $direction = 'desc') {
		$app = JFactory::getApplication();
		$context = $this->context;

		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);

		$id_menu = $this->getUserStateFromRequest($this->context . '.filter.id_menu', 'filter_menu', '');
		$this->setState('filter.id_menu', $id_menu);

		// List state information.
		parent::populateState($ordering, $direction);
	}

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('i.*, m.alias, m.title, m.menutype')
                ->from($db->quoteName('#__w7seouplifter_items', 'i'))
				->join('LEFT', $db->quoteName('#__menu', 'm') . ' ON m.id = i.id_menu')
                ->where('m.client_id = 0 AND m.id != 1');

		// Filter: like / search
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			$like = $db->quote('%' . $search . '%');
			$query->where('m.title LIKE ' . $like);
		}

		// Filter by published state
		$published = $this->getState('filter.published');

		if (is_numeric($published))
		{
			$query->where('i.published = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(i.published IN (0, 1))');
		}

		$id_menu = $this->getState('filter.id_menu');
		if($id_menu) {
			$query->where('m.menutype = \''.$id_menu.'\'');
		}

		// Add the list ordering clause.
		$orderCol	= $this->state->get('list.ordering', 'title');
		$orderDirn 	= $this->state->get('list.direction', 'asc');

		$query->order($db->escape($orderCol) . ' ' . $db->escape($orderDirn));

		return $query;
	}

	/**
	 * Method to import Joomla menu items
	 * 
	 * @return	void
	 */
	public function import()
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('i.id_menu')
                ->from($db->quoteName('#__w7seouplifter_items', 'i'));

		$db->setQuery((string)$query);
		$existing = $db->loadColumn(0);

		$query = $db->getQuery(true);
		$query->select('m.*')
				->from($db->quoteName('#__menu', 'm'))
				->where($db->quoteName('m.client_id') . ' = ' . 0)
				->where($db->quoteName('m.id') . ' != ' . 1);
		$db->setQuery((string)$query);

		$data = $db->loadObjectList();

		$i = 0;

		foreach($data as $item) {
			if(!in_array($item->id, $existing)) {
				$params = new JRegistry;
				$params->loadString($item->params, 'JSON');
				$item->params = $params;
	
				$columns = array('id_menu', 'item_description', 'item_title', 'item_keywords', 'modified', 'published');
				$values = array($item->id, $db->quote($item->params->get('menu-meta_description')), $db->quote($item->params->get('page_title')), $db->quote($item->params->get('menu-meta_keywords')), $db->quote(date("Y-m-d H:i:s")), 1);
	
				$import = $db->getQuery(true);
				$import
					->insert($db->quoteName('#__w7seouplifter_items'))
					->columns($db->quoteName($columns))
					->values(implode(',', $values));
	
				$db->setQuery((string)$import);
				$db->execute();
				$i++;
			}
		}

		return $i;

	}
}