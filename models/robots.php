<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\Registry\Registry;
use Joomla\Utilities\ArrayHelper; 

/**
 * Robots Model
 *
 * @since  0.0.1
 */
class W7SeoUplifterModelRobots extends JModelAdmin
{

    /**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{

		$access = JHelperContent::getActions('com_w7seouplifter');
		// Get the form.
		$form = $this->loadForm(
			'com_w7seouplifter.robots',
			'robots',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form)) {
			return false;
		}

		return $form;
	}

    /**
	 * Method to get the robots content.
	 *
	 * This method will load the global configuration data straight from
	 * JConfig. If configuration data has been saved in the session, that
	 * data will be merged into the original data, overwriting it.
	 *
	 * @return	object  An array containing all global config data.
	 *
	 * @since	1.6
	 */
	public function getData()
	{

        $item = new \stdClass;

        $filePath = JPATH_ROOT . '/robots.txt';

        if (!JFile::exists($filePath)) {
            \JLog::add(\JText::_('COM_W7SEOUPLIFTER_NO_ROBOTS_FILE'), \JLog::WARNING, 'jerror');
    		return;
        }

        $robots = file_get_contents($filePath);
        $item->robots = $robots;

		return $item;
	}

    /**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState(
			'com_w7seouplifter.robots.data',
			array()
		);

		if (empty($data))
		{
			$data = $this->getData();
		}

		return $data;
	}

    /**
	 * Method to override the JModelAdmin save() function to handle Save as Copy correctly
	 *
	 * @param   array The W7 SEO Uplifter redirect record data submitted from the form.
	 *
	 * @return  boolean
	 */
	public function save($data)
	{
		$input  = JFactory::getApplication()->input;
		$filter = JFilterInput::getInstance();
		
		$filePath = JPATH_ROOT . '/robots.txt';

        if (JFile::exists($filePath)) {
            JFile::delete($filePath);
        }

        JFile::append($filePath, $data['robots']);

        return true;
	}
}