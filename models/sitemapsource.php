<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\Registry\Registry;
jimport('joomla.application.component.controller');

/**
 * Sitemap Source Model
 *
 * @since  0.0.1
 */
class W7SeoUplifterModelSitemapsource extends JModelAdmin
{

	/**
	 * Method to override getItem to allow us to convert the JSON-encoded image information
	 * in the database record into an array for subsequent prefilling of the edit form
	 */
	public function getItem($pk = null)
	{
		$item = parent::getItem($pk);
		if ($item AND property_exists($item, 'params'))
		{
			$registry = new Registry($item->params);
			$item->params = $registry->toArray();
		}

		return $item; 
	}

	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   1.6
	 */
	public function getTable($type = 'Sitemap', $prefix = 'W7SeoUplifterTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

    /**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm(
			'com_w7seouplifter.sitemapsource',
			'sitemapsource',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);

		$input  = JFactory::getApplication()->input;
		$id = $input->get('id', '', 'INT');

		if (empty($form))
		{
			return false;
		}

		if($id != 0) {
			$form->setFieldAttribute('type', 'readonly', 'true');
		}

		if (JComponentHelper::isEnabled('com_djclassifieds', true))
		{
			$exclude_dj_items = new SimpleXMLElement('
				<field 
					name="exclude_dj_item"
					type="DjClassifiedsItems"
					default=""
					multiple="true"
					label="COM_W7SEOUPLIFTER_EXCLUDE_ITEM_LABEL"
					description="COM_W7SEOUPLIFTER_EXCLUDE_ITEM_DESC"
					class="hide_on_type djitems"
				/>
			');
			$form->setField($exclude_dj_items, 'params', true, 'sourceParams');

			$exclude_dj_category = new SimpleXMLElement('
				<field 
					name="exclude_dj_category"
					type="CustomCategories"
					extension="com_djclassifieds"
					label="COM_W7SEOUPLIFTER_EXCLUDE_CATEGORIES_LABEL"
					description="COM_W7SEOUPLIFTER_EXCLUDE_CATEGORIES_DESC"
					multiple="true"
					class="hide_on_type djcategory"
				/>
			');
			$form->setField($exclude_dj_category, 'params', true, 'sourceParams');
		}

		if (JComponentHelper::isEnabled('com_k2', true))
		{
			$exclude_k2_category = new SimpleXMLElement('
				<field 
					name="exclude_k2_category"
					type="CustomCategories"
					extension="com_k2"
					label="COM_W7SEOUPLIFTER_EXCLUDE_CATEGORIES_LABEL"
					description="COM_W7SEOUPLIFTER_EXCLUDE_CATEGORIES_DESC"
					multiple="true"
					class="hide_on_type k2category"
				/>
			');
			$form->setField($exclude_k2_category, 'params', true, 'sourceParams');
		}

		return $form;
	}

    /**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState(
			'com_w7seouplifter.edit.sitemapsource.data',
			array()
		);

		if (empty($data))
		{
			$data = $this->getItem();
		}

		return $data;
	}

	/**
	 * Method to override the JModelAdmin save() function
	 *
	 * @param   array The menu item record data submitted from the form.
	 *
	 * @return  parent::save() return value
	 */
	public function save($data)
	{
		$input  = JFactory::getApplication()->input;
		$filter = JFilterInput::getInstance();

		if($data['type'] != 'menu' && $this->checkIfExists($data['type']) && $data['id'] == 0) {
			$this->setError(JText::_('COM_W7SEOUPLIFTER_SITEMAP_SOURCE_EXISTS'));
    		return false;
		}

		return parent::save($data);
	}

	/**
	 * Method to change the published state of one or more records.
	 *
	 * @param   array    &$pks   A list of the primary keys to change.
	 * @param   integer  $value  The value of the published state.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since   1.6
	 */
	public function publish(&$pks, $value = 1)
	{
		$dispatcher = \JEventDispatcher::getInstance();
		$user = \JFactory::getUser();
		$table = $this->getTable();
		$pks = (array) $pks;

		// Include the plugins for the change of state event.
		\JPluginHelper::importPlugin($this->events_map['change_state']);

		// Access checks.
		foreach ($pks as $i => $pk)
		{
			$table->reset();

			if ($table->load($pk))
			{
				if (!$this->canEditState($table))
				{
					// Prune items that you can't change.
					unset($pks[$i]);

					\JLog::add(\JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'), \JLog::WARNING, 'jerror');

					return false;
				}

				// If the table is checked out by another user, drop it and report to the user trying to change its state.
				if (property_exists($table, 'checked_out') && $table->checked_out && ($table->checked_out != $user->id))
				{
					\JLog::add(\JText::_('JLIB_APPLICATION_ERROR_CHECKIN_USER_MISMATCH'), \JLog::WARNING, 'jerror');

					// Prune items that you can't change.
					unset($pks[$i]);

					return false;
				}

				/**
				 * Prune items that are already at the given state.  Note: Only models whose table correctly
				 * sets 'published' column alias (if different than published) will benefit from this
				 */
				$publishedColumnName = $table->getColumnAlias('published');

				if (property_exists($table, $publishedColumnName) && $table->get($publishedColumnName, $value) == $value)
				{
					unset($pks[$i]);

					continue;
				}
			}
		}

		// Check if there are items to change
		if (!count($pks))
		{
			return true;
		}

		// Attempt to change the state of the records.
		if (!$table->publish($pks, $value, $user->get('id')))
		{
			$this->setError($table->getError());

			return false;
		}

		$context = $this->option . '.' . $this->name;

		// Trigger the change state event.
		$result = $dispatcher->trigger($this->event_change_state, array($context, $pks, $value));

		if (in_array(false, $result, true))
		{
			$this->setError($table->getError());

			return false;
		}

		// Clear the component's cache
		$this->cleanCache();

		return true;
	}

	public function delete(&$recs = array()) 
	{
		$table = $this->getTable();

		foreach($recs as $id) {
			$table->delete($id);
		}
	}

	/**
	 * Check if the source type already exists
	 * 
	 * @param   string   $value   Menu item ID
	 *
	 * @return  boolean  Exits/Doesn't exist
	 */
	private function checkIfExists(string $value)
	{

		$db = JFactory::getDbo();
		$query = $db
			->getQuery(true)
			->select('i.id')
			->from($db->quoteName('#__w7seouplifter_sitemap_sources', 'i'))
			->where($db->quoteName('i.type') . " = " . $db->quote($value));

		$db->setQuery($query);
		$exists = $db->loadResult();

		if(!empty($exists)) {
			return true;
		} else {
			return false;
		}
	}

}