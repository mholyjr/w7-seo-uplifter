<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\Registry\Registry;
use Joomla\Utilities\ArrayHelper; 
use Joomla\CMS\Log\Log;

/**
 * Performance Model
 *
 * @since  0.0.1
 */
class W7SeoUplifterModelPerformance extends JModelAdmin
{
	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   1.6
	 */
	public function getTable($type = 'Performance', $prefix = 'W7SeoUplifterTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

    /**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{

		$access = JHelperContent::getActions('com_w7seouplifter');
		// Get the form.
		$form = $this->loadForm(
			'com_w7seouplifter.performance',
			'performance',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form)) {
			return false;
		}

		if(!$access->get('core.edit')) {

			// Images
			$form->setFieldAttribute('optimize_images', 'disabled', 'true', 'images');
			$form->setFieldAttribute('set_max_width', 'disabled', 'true', 'images');
			$form->setFieldAttribute('image_quality', 'disabled', 'true', 'images');
			$form->setFieldAttribute('image_max_width', 'disabled', 'true', 'images');
			$form->setFieldAttribute('use_webp', 'disabled', 'true', 'images');
			$form->setFieldAttribute('use_retina', 'disabled', 'true', 'images');
			$form->setFieldAttribute('retina_suffix', 'disabled', 'true', 'images');
			$form->setFieldAttribute('use_lazyload', 'disabled', 'true', 'images');
			$form->setFieldAttribute('cache_time', 'disabled', 'true', 'images');

			// Minify
			$form->setFieldAttribute('enable_minify_scripts', 'disabled', 'true', 'minify');
			$form->setFieldAttribute('enable_minify_styles', 'disabled', 'true', 'minify');
		}

		return $form;
	}

	/**
	 * Method to override the JModelAdmin save() function to handle Save as Copy correctly
	 *
	 * @param   array The W7 SEO Uplifter redirect record data submitted from the form.
	 *
	 * @return  parent::save() return value
	 */
	public function save($data)
	{
		$input  = JFactory::getApplication()->input;
		$filter = JFilterInput::getInstance();
		
		return parent::save($data);
	}

	/**
	 * Method to get the configuration data.
	 *
	 * This method will load the global configuration data straight from
	 * JConfig. If configuration data has been saved in the session, that
	 * data will be merged into the original data, overwriting it.
	 *
	 * @return	array  An array containing all global config data.
	 *
	 * @since	1.6
	 */
	public function getData()
	{
		$db = JFactory::getDbo();
		$query = $db
			->getQuery(true)
			->select('i.*')
			->from($db->quoteName('#__w7seouplifter_performance', 'i'));;

		$db->setQuery((string)$query);
		$item = $db->loadObject();

		if ($item AND property_exists($item, 'images'))
		{
			$registry = new Registry($item->images);
			$item->images = $registry->toArray();
		}

		if ($item AND property_exists($item, 'minify'))
		{
			$registry = new Registry($item->minify);
			$item->minify = $registry->toArray();
		}

		$data = ArrayHelper::fromObject($item);

		return $item;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState(
			'com_w7seouplifter.performance.data',
			array()
		);

		if (empty($data))
		{
			$data = $this->getData();
		}

		return $data;
	}
}