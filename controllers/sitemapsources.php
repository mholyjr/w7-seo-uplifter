<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.folder');

use Joomla\Utilities\ArrayHelper;
use Joomla\CMS\Log\Log;

/**
 * Sitemap Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 * @since       0.0.1
 */
class W7SeoUplifterControllerSitemapsources extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JControllerLegacy
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Sitemapsources', $prefix = 'W7SeoUplifterModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

    /**
	 * Default save method override
	 * 
	 * @return void
	 */
    public function save()
    {
        $jinput = JFactory::getApplication()->input;
        $data = $jinput->post->get('jform', array(), 'array');

        $this->getModel()->save($data);
		$this->setMessage(\JText::_('COM_W7SEOUPLIFTER_SITEMAP_SAVED'));
        $this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list . $extensionURL, false));

    }

	/**
	 * Method to publish a list of items
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public function publish()
	{
		// Check for request forgeries
		$this->checkToken();

		// Get items to publish from the request.
		$cid = $this->input->get('cid', array(), 'array');
		$data = array('publish' => 1, 'unpublish' => 0, 'archive' => 2, 'trash' => -2, 'report' => -3);
		$task = $this->getTask();
		$value = ArrayHelper::getValue($data, $task, 0, 'int');

		if (empty($cid))
		{
			\JLog::add(\JText::_($this->text_prefix . '_NO_ITEM_SELECTED'), \JLog::WARNING, 'jerror');
		}
		else
		{
			// Get the model.
			$model = $this->getModel('Sitemapsource', 'W7SeoUplifterModel', array()); 

			// Make sure the item ids are integers
			$cid = ArrayHelper::toInteger($cid);

			// Publish the items.
			try
			{
				$model->publish($cid, $value);
				$errors = $model->getErrors();
				$ntext = null;

				if ($value === 1)
				{
					if ($errors)
					{
						\JFactory::getApplication()->enqueueMessage(\JText::plural($this->text_prefix . '_N_ITEMS_FAILED_PUBLISHING', count($cid)), 'error');
					}
					else
					{
						$ntext = $this->text_prefix . '_N_ITEMS_PUBLISHED';
					}
				}
				elseif ($value === 0)
				{
					$ntext = $this->text_prefix . '_N_ITEMS_UNPUBLISHED';
				}
				elseif ($value === 2)
				{
					$ntext = $this->text_prefix . '_N_ITEMS_ARCHIVED';
				}
				else
				{
					$ntext = $this->text_prefix . '_N_ITEMS_TRASHED';
				}

				if ($ntext !== null)
				{
					$this->setMessage(\JText::plural($ntext, count($cid)));
				}
			}
			catch (\Exception $e)
			{
				$this->setMessage($e->getMessage(), 'error');
			}
		}

		$extension = $this->input->get('extension');
		$extensionURL = $extension ? '&extension=' . $extension : '';
		$this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list . $extensionURL, false));
	}

	public function delete()
	{
		// Check for request forgeries
		$this->checkToken();

		$cid = $this->input->get('cid', array(), 'array');
		$nrecs 		= $this->input->get('boxchecked', 0, 'INT');

		if (empty($cid))
		{
			\JLog::add(\JText::_($this->text_prefix . '_NO_ITEM_SELECTED'), \JLog::WARNING, 'jerror');
		}
		else
		{
			$model = $this->getModel('Sitemapsource', 'W7SeoUplifterModel', array()); 

			$cid = ArrayHelper::toInteger($cid);
		
			$model->delete($cid);

			$msg = $nrecs . 'record(s) deleted';
			$this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list . $extensionURL, false));
		}
	}

	public function trash()
	{
		$input  	= JFactory::getApplication()->input;
		$recs 		= $input->get('cid', array(), 'ARRAY');
		$nrecs 		= $input->get('boxchecked', 0, 'INT');

		if($recs) {
			$model = $this->getModel('Sitemapsource', 'W7SeoUplifterModel', array()); 
		
			$model->trash($recs);

			$msg = $nrecs . 'record(s) deleted';
			$this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list . $extensionURL, false));
		}
	}

    /**
     * Method to generate sitemap
     * 
     * @return void
     */
    public function generate()
    {

        $this->getModel()->generateSitemap();

        //$model->generateSitemap();

        $this->setMessage(\JText::_('COM_W7SEOUPLIFTER_HTACCESS_SAVED'));
        $this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=dashboard' . $extensionURL, false));
    }

}