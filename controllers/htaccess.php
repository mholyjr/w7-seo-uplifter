<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.folder');

use Joomla\Utilities\ArrayHelper;
use Joomla\CMS\Log\Log;

/**
 * Htaccess Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 * @since       0.0.1
 */
class W7SeoUplifterControllerHtaccess extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JControllerLegacy
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Htaccess', $prefix = 'W7SeoUplifterModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	/**
	 * Default save method override
	 * 
	 * @return void
	 */
    public function save()
    {
        $jinput = JFactory::getApplication()->input;
        $data = $jinput->post->get('jform', array(), 'array');

        $this->getModel()->save($data);
		$this->setMessage(\JText::_('COM_W7SEOUPLIFTER_HTACCESS_SAVED'));
        $this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list . $extensionURL, false));
    }

    /**
     * Method to restore .htaccess
     * 
     * @return void
     */
    public function restore()
    {
        $model = $this->getModel()->restore();

        if($model === true) {
            $this->setMessage(\JText::_('COM_W7SEOUPLIFTER_HTACCESS_RESTORED'));
        }

        $this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list . $extensionURL, false));
    }

    /**
     * Method to delete .htaccess
     * 
     * @return void
     */
    public function delete()
    {
        $model = $this->getModel()->deleteFile();

        if($model === true) {
            $this->setMessage(\JText::_('COM_W7SEOUPLIFTER_HTACCESS_DELETED'));
        }

        $this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list . $extensionURL, false));
    }

}