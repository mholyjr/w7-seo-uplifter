<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/**
 * Sitemap Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 * @since       0.0.9
 */
class W7SeoUplifterControllerSitemap extends JControllerForm
{

    /**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Sitemap', $prefix = 'W7SeoUplifterModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
    }

    /**
     * Method to generate XML sitemap
     * 
     * @return void
     */
    public function generateXml()
    {
        $model = $this->getModel();

        $sitemap = $model->generateSitemap();

        if($sitemap === true) {
            $this->setMessage(\JText::_('COM_W7SEOUPLIFTER_SITEMAP_GENERATED_SUCCESS'));
        }

        $this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=sitemapsources', false));
    }
    
}