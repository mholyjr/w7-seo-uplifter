<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.folder');

use Joomla\Utilities\ArrayHelper;
use Joomla\CMS\Log\Log;

/**
 * Performance Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 * @since       0.0.1
 */
class W7SeoUplifterControllerPerformance extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JControllerLegacy
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Performance', $prefix = 'W7SeoUplifterModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	/**
	 * Default save method override
	 * 
	 * @return void
	 */
    public function save()
    {
        $jinput = JFactory::getApplication()->input;
        $data = $jinput->post->get('jform', array(), 'array');

        $this->getModel()->save($data);
		$this->setMessage(\JText::_('COM_W7SEOUPLIFTER_PERFORMANCE_SAVED'));
        $this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list . $extensionURL, false));

    }

	/**
	 * Method to purge the images cache
	 * 
	 * @return void
	 */
	public function purge()
	{
		JFolder::delete(JPATH_ROOT. '/media/com_w7seouplifter/images');

		$this->setMessage(\JText::_('COM_W7SEOUPLIFTER_PERFORMANCE_IMAGES_PURGED'));
        $this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list . $extensionURL, false));
	}

}