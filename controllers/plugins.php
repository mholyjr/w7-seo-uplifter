<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\Utilities\ArrayHelper;

/**
 * Plugins Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 * @since       0.0.1
 */
class W7SeoUplifterControllerPlugins extends JControllerAdmin
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JControllerLegacy
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Plugins', $prefix = 'W7SeoUplifterModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

    public function toggle()
    {
		$id = JFactory::getApplication()->input->get('id', '', 'string');

		$task = $this->getTask();

		if (empty($id)) {
			\JLog::add(\JText::_('COM_W7SEOUPLIFTER_ENABLE_PLUGIN_NO_ID'), \JLog::WARNING, 'jerror');
		} else {
			$model = $this->getModel('Plugins', 'W7SeoUplifterModel', array());
            $newVal = $model->togglePlugin($id);

            $newVal == 1 ? $this->setMessage(\JText::_('COM_W7SEOUPLIFTER_PLUGIN_ENABLED')) : $this->setMessage(\JText::_('COM_W7SEOUPLIFTER_PLUGIN_DISABLED'));
            
        }

        $this->setRedirect(\JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list . $extensionURL, false));
    }
}