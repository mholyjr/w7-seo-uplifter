DROP TABLE IF EXISTS `#__w7seouplifter_items`;

DROP TABLE IF EXISTS `#__w7seouplifter_performance`;

DROP TABLE IF EXISTS `#__w7seouplifter_redirects`;

DROP TABLE IF EXISTS `#__w7seouplifter_sitemap_sources`;

DROP TABLE IF EXISTS `#__w7seouplifter_sitemap_types`;
