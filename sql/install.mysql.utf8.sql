DROP TABLE IF EXISTS `#__w7seouplifter_items`;
CREATE TABLE `#__w7seouplifter_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL,
  `item_description` text,
  `item_title` text,
  `item_keywords` text,
  `modified` datetime DEFAULT NULL,
  `published` int(1) DEFAULT '1',
  `facebook` text,
  `twitter` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `#__w7seouplifter_performance`;
CREATE TABLE `#__w7seouplifter_performance` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `images` text NOT NULL,
  `minify` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci;

INSERT INTO `#__w7seouplifter_performance` (`id`, `images`, `minify`)
VALUES
	(1,'{\"optimize_images\":\"1\",\"set_max_width\":\"1\",\"image_max_width\":\"800\",\"image_quality\":\"50\",\"use_webp\":\"1\",\"use_retina\":\"0\",\"retina_suffix\":\"@2x\",\"use_lazyload\":\"0\",\"cache_time\":\"6\"}','{\"enable_minify_scripts\":\"1\",\"enable_minify_styles\":\"1\"}');

DROP TABLE IF EXISTS `#__w7seouplifter_redirects`;
CREATE TABLE `#__w7seouplifter_redirects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from` varchar(1024) NOT NULL DEFAULT '',
  `to` varchar(1024) NOT NULL DEFAULT '',
  `type` int(1) NOT NULL,
  `redirect_status` int(11) NOT NULL,
  `published` int(1) NOT NULL DEFAULT '1',
  `note` text,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `#__w7seouplifter_sitemap_sources`;
CREATE TABLE `#__w7seouplifter_sitemap_sources` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `params` text,
  `published` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `#__w7seouplifter_sitemap_sources_types`;
CREATE TABLE `#__w7seouplifter_sitemap_sources_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci;

INSERT INTO `#__w7seouplifter_sitemap_sources_types` (`id`, `type`, `title`)
VALUES
	(1,'menu','COM_W7SEOUPLIFTER_SITEMAP_TYPE_MENU'),
  (2,'joomlaarticles','COM_W7SEOUPLIFTER_SITEMAP_TYPE_JOOMLA_ARTICLES'),
  (3,'joomlacategories','COM_W7SEOUPLIFTER_SITEMAP_TYPE_JOOMLA_CATEGORIES'),
  (4,'joomlatags','COM_W7SEOUPLIFTER_SITEMAP_TYPE_JOOMLA_TAGS'),
  (5,'djclassifiedsitems','COM_W7SEOUPLIFTER_SITEMAP_TYPE_DJCLASSIFIEDS_ITEMS'),
  (6,'djclassifiedscategories','COM_W7SEOUPLIFTER_SITEMAP_TYPE_DJCLASSIFIEDS_CATEGORIES'),
  (7,'djclassifiedsprofiles','COM_W7SEOUPLIFTER_SITEMAP_TYPE_DJCLASSIFIEDS_PROFILES'),
  (8,'k2articles','COM_W7SEOUPLIFTER_SITEMAP_TYPE_K2_ARTICLES'),
  (9,'k2categories','COM_W7SEOUPLIFTER_SITEMAP_TYPE_K2_CATEGORIES');