DROP TABLE IF EXISTS `#__w7seouplifter_sitemap_sources`;
CREATE TABLE `#__w7seouplifter_sitemap_sources` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `params` text,
  `published` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `#__w7seouplifter_sitemap_sources_types`;
CREATE TABLE `#__w7seouplifter_sitemap_sources_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci;

INSERT INTO `#__w7seouplifter_sitemap_sources_types` (`id`, `type`, `title`)
VALUES
    (1,'menu','COM_W7SEOUPLIFTER_SITEMAP_TYPE_MENU'),
    (2,'joomlaarticles','COM_W7SEOUPLIFTER_SITEMAP_TYPE_JOOMLA_ARTICLES'),
    (3,'joomlacategories','COM_W7SEOUPLIFTER_SITEMAP_TYPE_JOOMLA_CATEGORIES');