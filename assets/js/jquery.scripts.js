jQuery(function() {
    
    setInputs()

    jQuery("#jform_type").on('change', function() {
        setInputs()
    });

    function setInputs() {
        var category_opts = ['Joomla articles', 'Joomla categories'];
        var k2_category_opts = ['K2 articles', 'K2 categories'];
        var tags_opts = ['Joomla tags'];
        var menu_opts = ['Menu'];
        var dj_items_opts = ['DJ Classifieds items'];
        var dj_category_opts = ['DJ Classifieds items', 'DJ Classifieds categories'];
        var dj_profiles_opts = ['DJ Classifieds profiles'];

        var sourcetype = jQuery("#jform_type option:selected").text();
        var categories = jQuery('.hide_on_type.category').parent('.controls').parent('.control-group');
        var menus = jQuery('.hide_on_type.menu').parent('.controls').parent('.control-group');
        var tags = jQuery('.hide_on_type.tags').parent('.controls').parent('.control-group');
        var djitems = jQuery('.hide_on_type.djitems').parent('.controls').parent('.control-group');
        var djcategories = jQuery('.hide_on_type.djcategory').parent('.controls').parent('.control-group');
        var users = jQuery('.hide_on_type.user').parent('.controls').parent('.control-group');
        var k2categories = jQuery('.hide_on_type.k2category').parent('.controls').parent('.control-group');

        if(category_opts.includes(sourcetype)) {
            categories.fadeIn('fast')
        } else {
            categories.fadeOut('fast')
        }

        if(menu_opts.includes(sourcetype)) {
            menus.fadeIn('fast')
        } else {
            menus.fadeOut('fast')
        }

        if(tags_opts.includes(sourcetype)) {
            tags.fadeIn('fast')
        } else {
            tags.fadeOut('fast')
        }

        if(dj_items_opts.includes(sourcetype)) {
            djitems.fadeIn('fast')
        } else {
            djitems.fadeOut('fast')
        }

        if(dj_category_opts.includes(sourcetype)) {
            djcategories.fadeIn('fast');
        } else {
            djcategories.fadeOut('fast');
        }

        if(dj_profiles_opts.includes(sourcetype)) {
            users.fadeIn('fast');
        } else {
            users.fadeOut('fast');
        }

        if(k2_category_opts.includes(sourcetype)) {
            k2categories.fadeIn('fast');
        } else {
            k2categories.fadeOut('fast');
        }
    }
})

// [{"field":"jform[type]","values":["20"],"sign":"=","op":""}]