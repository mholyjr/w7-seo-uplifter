function copyToClipboard(elementID, tooltipId) {
    let element = document.getElementById(elementID);
    let tooltip = document.getElementById(tooltipId);
    let elementText = element.value; 
    copyText(elementText); 

    tooltip.innerHTML = 'Copied to clipboard!'

    setTimeout(function(){ tooltip.innerHTML = 'Copy to clipboard' }, 1000);
}

function copyText(text) {
    navigator.clipboard.writeText(text);
}