<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * W7 SEO Uplifter component helper.
 *
 * @param   string  $submenu  The name of the active view.
 *
 * @return  void
 *
 * @since   1.6
 */
class W7SeoUplifterStatsHelper extends JHelperContent
{
    /**
     * Method to show how many items are imported
     * 
     * @return  mixed    Array of data
     */
    public static function statsImported()
    {
        $db = JFactory::getDbo();
		$query = $db
			->getQuery(true)
			->select('COUNT(' . $db->quoteName('i.id') . ')')
			->from($db->quoteName('#__w7seouplifter_items', 'i'));

        $totalQuery = $db
			->getQuery(true)
			->select('COUNT(' . $db->quoteName('m.id') . ')')
			->from($db->quoteName('#__menu', 'm'))
            ->where($db->quoteName('m.id') . ' != ' . 1)
            ->where($db->quoteName('m.client_id') . ' = ' . 0);

		$db->setQuery((string)$query);
        $imported = $db->loadResult();

        $db->setQuery((string)$totalQuery);
        $total = $db->loadResult();

        if($total == $imported) {
            $check = 1;
        } else {
            $check = 0;
        }

        return ['total' => $total, 'imported' => $imported, 'check' => $check];
    }

    /**
     * Method to show how many items are missing Page title
     * 
     * @return  mixed    Array of data
     */
    public static function statsTitles()
    {
        $db = JFactory::getDbo();
		$query = $db
			->getQuery(true)
			->select('COUNT(' . $db->quoteName('i.id') . ')')
			->from($db->quoteName('#__w7seouplifter_items', 'i'))
            ->where($db->quoteName('i.item_title') . 'IS NULL OR ' . $db->quoteName('i.item_title') . ' = ""');

        $totalQuery = $db
            ->getQuery(true)
            ->select('COUNT(' . $db->quoteName('i.id') . ')')
            ->from($db->quoteName('#__w7seouplifter_items', 'i'));

		$db->setQuery((string)$query);
        $missing = $db->loadResult();

        $db->setQuery((string)$totalQuery);
        $total = $db->loadResult();

        if($missing == 0) {
            $check = 1;
        } else {
            $check = 0;
        }

        return ['total' => $total, 'missing' => $missing, 'check' => $check];
    }

    /**
     * Method to show how many items are missing Page description
     * 
     * @return  mixed    Array of data
     */
    public static function statsDescription()
    {
        $db = JFactory::getDbo();
		$query = $db
			->getQuery(true)
			->select('COUNT(' . $db->quoteName('i.id') . ')')
			->from($db->quoteName('#__w7seouplifter_items', 'i'))
            ->where($db->quoteName('i.item_description') . 'IS NULL OR ' . $db->quoteName('i.item_description') . ' = ""');

        $totalQuery = $db
            ->getQuery(true)
            ->select('COUNT(' . $db->quoteName('i.id') . ')')
            ->from($db->quoteName('#__w7seouplifter_items', 'i'));

		$db->setQuery((string)$query);
        $missing = $db->loadResult();

        $db->setQuery((string)$totalQuery);
        $total = $db->loadResult();

        if($missing == 0) {
            $check = 1;
        } else {
            $check = 0;
        }

        return ['total' => $total, 'missing' => $missing, 'check' => $check];
    }

    /**
     * Method to show how many items are missing Page social cards
     * 
     * @return  mixed    Array of data
     */
    public static function statsSocial()
    {
        $db = JFactory::getDbo();
		$query = $db
			->getQuery(true)
			->select('COUNT(' . $db->quoteName('i.id') . ')')
			->from($db->quoteName('#__w7seouplifter_items', 'i'))
            ->where($db->quoteName('i.facebook') . 'IS NULL OR ' . $db->quoteName('i.facebook') . ' = "" OR ' . $db->quoteName('i.twitter') . 'IS NULL OR ' . $db->quoteName('i.twitter') . ' = ""');

        $totalQuery = $db
            ->getQuery(true)
            ->select('COUNT(' . $db->quoteName('i.id') . ')')
            ->from($db->quoteName('#__w7seouplifter_items', 'i'));

		$db->setQuery((string)$query);
        $missing = $db->loadResult();

        $db->setQuery((string)$totalQuery);
        $total = $db->loadResult();

        if($missing == 0) {
            $check = 1;
        } else {
            $check = 0;
        }

        return ['total' => $total, 'missing' => $missing, 'check' => $check];
    }
}