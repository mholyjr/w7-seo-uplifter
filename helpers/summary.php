<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * W7 SEO Uplifter component helper.
 *
 * @param   string  $submenu  The name of the active view.
 *
 * @return  void
 *
 * @since   1.6
 */
class W7SeoUplifterSummaryHelper extends JHelperContent
{
    /**
     * Check if the title meets the requirements 
     * 
     * @param   string  $title
     * 
     * @return  mixed   Array of values to render in the Item edit view
     */
    public function checkTitle($title)
    {
        $length = strlen($title);
        $res = array();

        if(!empty($title)) {

            $duplicates = $this->checkDuplicate($title, 'item_title');

            if($length < 40) {
                $res[] = ['type' => 'danger', 'icon' => 'bi-x-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_TITLE_DANGER_TOO_SHORT') . ' ('.$length.')'];
            } else if($length >= 40 && $length < 50) {
                $res[] = ['type' => 'warning', 'icon' => 'bi-exclamation-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_TITLE_WARNING_TOO_SHORT') . ' ('.$length.')'];
            } else if($length >= 50 && $length <= 65) {
                $res[] = ['type' => 'success', 'icon' => 'bi-check-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_TITLE_SUCCESS_LENGTH') . ' ('.$length.')'];
            } else if($length > 65 && $length <= 70) {
                $res[] = ['type' => 'warning', 'icon' => 'bi-exclamation-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_TITLE_WARNING_TOO_LONG') . ' ('.$length.')'];
            } else if($length > 70) {
                $res[] = ['type' => 'danger', 'icon' => 'bi-x-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_TITLE_TOO_LONG') . ' ('.$length.')'];
            }
    
            if($duplicates == 1) {
                $res[] = ['type' => 'success', 'icon' => 'bi-check-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_TITLE_IS_UNIQUE')];
            } else {
                $res[] = ['type' => 'danger', 'icon' => 'bi-x-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_TITLE_NOT_UNIQUE')];
            }
        } else {
            $res[] = ['type' => 'danger', 'icon' => 'bi-x-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_TITLE_NOT_SET')];
        }
        

        return $res;
    }

    /**
     * Check if the description meets the requirements 
     * 
     * @param   string  $desc
     * 
     * @return  mixed   Array of values to render in the Item edit view
     */
    public function checkDescription($desc)
    {
        $length = strlen($desc);
        $res = array();

        if(!empty($desc)) {

            $duplicates = $this->checkDuplicate($desc, 'item_description');

            if($length < 140) {
                $res[] = ['type' => 'danger', 'icon' => 'bi-x-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_DESCRIPTION_DANGER_TOO_SHORT') . ' ('.$length.')'];
            } else if($length >= 140 && $length < 150) {
                $res[] = ['type' => 'warning', 'icon' => 'bi-exclamation-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_DESCRIPTION_WARNING_TOO_SHORT') . ' ('.$length.')'];
            } else if($length >= 150 && $length <= 160) {
                $res[] = ['type' => 'success', 'icon' => 'bi-check-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_DESCRIPTION_SUCCESS_LENGTH') . ' ('.$length.')'];
            } else if($length > 160 && $length <= 170) {
                $res[] = ['type' => 'warning', 'icon' => 'bi-exclamation-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_DESCRIPTION_WARNING_TOO_LONG') . ' ('.$length.')'];
            } else if($length > 170) {
                $res[] = ['type' => 'danger', 'icon' => 'bi-x-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_DESCRIPTION_TOO_LONG') . ' ('.$length.')'];
            }
    
            if($duplicates == 1) {
                $res[] = ['type' => 'success', 'icon' => 'bi-check-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_DESCRIPTION_IS_UNIQUE')];
            } else {
                $res[] = ['type' => 'danger', 'icon' => 'bi-x-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_DESCRIPTION_NOT_UNIQUE')];
            }
        } else {
            $res[] = ['type' => 'danger', 'icon' => 'bi-x-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_DESCRIPTION_NOT_SET')];
        }
        

        return $res;
    }

    /**
     * Check if the social title meets the requirements 
     * 
     * @param   string  $title
     * 
     * @return  mixed   Array of values to render in the Item edit view
     */
    public function checkSocialTitle($title)
    {
        $length = strlen($title);
        $res = array();

        if(!empty($title)) {

            if($length < 40) {
                $res[] = ['type' => 'danger', 'icon' => 'bi-x-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_SOCIAL_TITLE_DANGER_TOO_SHORT') . ' ('.$length.')'];
            } else if($length >= 40 && $length < 50) {
                $res[] = ['type' => 'warning', 'icon' => 'bi-exclamation-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_SOCIAL_TITLE_WARNING_TOO_SHORT') . ' ('.$length.')'];
            } else if($length >= 50 && $length <= 65) {
                $res[] = ['type' => 'success', 'icon' => 'bi-check-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_SOCIAL_TITLE_SUCCESS_LENGTH') . ' ('.$length.')'];
            } else if($length > 65 && $length <= 70) {
                $res[] = ['type' => 'warning', 'icon' => 'bi-exclamation-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_SOCIAL_TITLE_WARNING_TOO_LONG') . ' ('.$length.')'];
            } else if($length > 70) {
                $res[] = ['type' => 'danger', 'icon' => 'bi-x-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_SOCIAL_TITLE_TOO_LONG') . ' ('.$length.')'];
            }

        } else {
            $res[] = ['type' => 'danger', 'icon' => 'bi-x-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_SOCIAL_TITLE_NOT_SET')];
        }

        return $res;
    }

    /**
     * Check if the social image is set
     * 
     * @param   string  $img
     * 
     * @return  mixed   Array of values to render in the Item edit view
     */
    public function checkSocialImage($img)
    {
        $res = array();
        if(!empty($img)) {
            $res[] = ['type' => 'success', 'icon' => 'bi-check-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_SOCIAL_IMAGE_IS_SET')];
        } else {
            $res[] = ['type' => 'danger', 'icon' => 'bi-x-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_SOCIAL_IMAGE_NOT_SET')];
        }

        return $res;
    }

    /**
     * Check if the social description is set
     * 
     * @param   string  $desc
     * 
     * @return  mixed   Array of values to render in the Item edit view
     */
    public function checkSocialDescription($desc)
    {
        $res = array();
        if(!empty($desc)) {
            $res[] = ['type' => 'success', 'icon' => 'bi-check-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_SOCIAL_DESCRIPTION_IS_SET')];
        } else {
            $res[] = ['type' => 'danger', 'icon' => 'bi-x-circle', 'message' => JText::_('COM_W7SEOUPLIFTER_SOCIAL_DESCRIPTION_NOT_SET')];
        }

        return $res;
    }

    /**
     * Check if the value has any duplicates
     * 
     * @param   string  $val    The value to check
     * @param   string  $type   Type of the value
     * 
     * @return  mixed   Array of values to render in the Item edit view
     */
    private function checkDuplicate(string $val, string $type)
    {
        $db = JFactory::getDbo();
        $query = $db
            ->getQuery(true)
            ->select('COUNT(i.' . $type . ')')
            ->from($db->quoteName('#__w7seouplifter_items', 'i'))
            ->where($db->quoteName('i.' . $type) . " = " . $db->quote($val));

        $db->setQuery($query);
        $count = $db->loadResult();

        return $count;
    }
}