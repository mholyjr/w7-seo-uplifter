<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7seouplifter
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * W7 SEO Uplifter component helper.
 *
 * @param   string  $submenu  The name of the active view.
 *
 * @return  void
 *
 * @since   1.6
 */
abstract class W7SeoUplifterHelper extends JHelperContent
{
	/**
	 * Configure the Linkbar.
	 *
	 * @return Bool
	 */

	public static function addSubmenu($submenu) 
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_W7SEOUPLIFTER_DASHBOARD'),
			'index.php?option=com_w7seouplifter&view=dashboard',
			$submenu == 'items'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_W7SEOUPLIFTER_ITEMS'),
			'index.php?option=com_w7seouplifter&view=items',
			$submenu == 'items'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_W7SEOUPLIFTER_PERFORMANCE'),
			'index.php?option=com_w7seouplifter&view=performance',
			$submenu == 'items'
		);

        JHtmlSidebar::addEntry(
			JText::_('COM_W7SEOUPLIFTER_REDIRECTS'),
			'index.php?option=com_w7seouplifter&view=redirects',
			$submenu == 'items'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_W7SEOUPLIFTER_SITEMAP_SOURCES'),
			'index.php?option=com_w7seouplifter&view=sitemapsources',
			$submenu == 'items'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_W7SEOUPLIFTER_HTACCESS_EDITOR'),
			'index.php?option=com_w7seouplifter&view=htaccess',
			$submenu == 'items'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_W7SEOUPLIFTER_ROBOTS_EDITOR'),
			'index.php?option=com_w7seouplifter&view=robots',
			$submenu == 'items'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_W7SEOUPLIFTER_PLUGINS_CHECKER'),
			'index.php?option=com_w7seouplifter&view=plugins',
			$submenu == 'items'
		);

		// Set some global property
        $document = JFactory::getDocument();
        
		if ($submenu == 'categories') 
		{
			$document->setTitle(JText::_('COM_HELLOWORLD_ADMINISTRATION_CATEGORIES'));
        }
	}

}